include .makedefault
-include .makerc

ENV = pipenv run
# SSH =
# PROD_SSH =
CONTAINER_NAME = unnamed_api
DB_HOST = 127.0.0.1
DB_PORT = 5432
DB_USER = postgres
DB_NAME = unnamed_db

# ===============================================
# LOCAL
# ===============================================
# локально: создать миграции
local-makemigrations:
	$(ENV) python manage.py makemigrations
# локально: применить мирации
local-migrate:
	$(ENV) python manage.py migrate
# локально: проверка кода и тесты
local-test:
	$(ENV) flake8
	$(ENV) python manage.py test
# локально: запустить
local-run:
	$(ENV) python manage.py runserver
# локально: создать суперюзера
local-createsuperuser:
	$(ENV) python manage.py createsuperuser
# локально: удалить все миграции
local-delete-migrations:
	find ./core/migrations/ -type f -not -name '__init__.py' -delete
# локально: пересоздать sqlite бд
local-reset-sqlite-db:
	-rm -f db.sqlite3
	make local-delete-migrations
	make local-makemigrations
	make local-migrate
# локально: пересоздать postgres бд
local-reset-pgsql-db:
	dropdb -h $(DB_HOST) -p $(DB_PORT) -U $(DB_USER) -W --if-exists $(DB_NAME)
	createdb -h $(DB_HOST) -p $(DB_PORT) -U $(DB_USER) -T template0 -W -E UTF8 -l ru_RU.UTF-8 $(DB_NAME)
	make local-migrate
	$(ENV) python manage.py init_admin
	$(ENV) python manage.py init_data

# ===============================================
# DOCKER
# ===============================================
# docker: собрать
docker-build:
	docker-compose build
# docker: запустить
docker-run:
	docker-compose up -d --build
	docker-compose ps
# docker: дропнуть базу
docker-dropdb:
	docker exec db dropdb -h ${DB_HOST} -p 5432 -U ${DB_USER} --if-exists ${DB_NAME}
# docker: создать базу
docker-createdb:
	docker exec db createdb -h ${DB_HOST} -p 5432 -U ${DB_USER} -T template0 -E UTF8 -l ru_RU.UTF-8 ${DB_NAME}

# ===============================================
# SERVER
# ===============================================
# Сервер: дропнуть базу
stage-dropdb:
	$(SSH) docker exec db dropdb -h ${DB_HOST} -p 5432 -U ${DB_USER} --if-exists ${DB_NAME}
# Сервер: создать базу
stage-createdb:
	$(SSH) docker exec db createdb -h ${DB_HOST} -p 5432 -U ${DB_USER} -T template0 -E UTF8 -l ru_RU.UTF-8 ${DB_NAME}