from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from core.models import User, Publication
from core.models.comment import Comment
from core.models.common import VerificationCode
from core.models.friendship import FriendshipRequest, Friendship


class DisableDeleteMixin(object):
    """
    Mixin для запрета операции удаления в админке, использовать совместно с ModelAdmin
    """
    @staticmethod
    def has_delete_permission(request, obj=None):
        return False


class ReadOnlyMixin(admin.ModelAdmin):
    """
    Mixin для read-only моделей, использовать совместно с ModelAdmin
    """
    readonly_fields = []

    def get_readonly_fields(self, request, obj=None):
        fields = [field.name for field in obj._meta.fields]
        many_to_many_fields = [field.name for field in obj._meta.many_to_many]
        return list(self.readonly_fields) + fields + many_to_many_fields

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class CustomUserAdmin(DisableDeleteMixin, UserAdmin):
    fieldsets = (
        (None, {'fields': ('password',)}),
        ('Персональная информация', {'fields': (
            'first_name', 'middle_name', 'last_name', 'email', 'username', 'phone', 'avatar', )}),
        ('Разрешения', {'fields': (
            'is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        ('Важные даты', {'fields': ('last_login', 'date_joined')}),
        ('Другое', {'fields': (
            'gender', 'birthday', )}),
    )
    # add_fieldsets - поля, которые будут отображаться на странице создания пользователя
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'first_name', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'username', 'phone', 'first_name', 'last_name', 'middle_name', 'is_staff')
    search_fields = ('phone', 'first_name', 'last_name', 'email', 'username',)
    ordering = ('email',)


class VerificationCodeAdmin(ReadOnlyMixin):
    readonly_fields = ('verification_code', 'code_type', 'email', 'phone', 'hash_uuid', 'validated', 'expired',
                       'ip_address', )


admin.site.register(User, CustomUserAdmin)
admin.site.register(VerificationCode, VerificationCodeAdmin)
admin.site.register(Publication)
admin.site.register(FriendshipRequest)
admin.site.register(Friendship)
admin.site.register(Comment)
