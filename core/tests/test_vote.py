import json

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone
from oauth2_provider.models import Application

from core.models import User, Publication
from core.models.comment import Comment
from core.models.vote import Vote
from core.serializers.comment import CommentSerializer
from core.serializers.publication import PublicationSerializer
from core.tests.utils import get_basic_auth_header


class CommentTests(TestCase):
    """
    Тест методов для оценки объектов (Публикаций, комментариев и тд)
    """
    def setUp(self):
        # Инициализация клиента
        self.host = 'localhost'
        self.client = Client(HTTP_HOST=self.host)

        self.email = 'arvarohfx@gmail.com'
        self.password = 'Str0ngPa$$w0rd'
        self.username = 'arvaroh'
        self.phone = '+79010003300'
        self.first_name = 'Дмитрий'
        self.middle_name = 'Сергеевич'
        self.last_name = 'Зотов'
        self.gender = 'MALE'
        self.birthday = timezone.now().date()

        self.user = User.objects.create_user(
            email=self.email,
            username=self.username,
            first_name=self.first_name,
            middle_name=self.middle_name,
            last_name=self.last_name,
            phone=self.phone,
            gender=self.gender,
            birthday=self.birthday,
            password=self.password
        )

        self.target_user = User.objects.create_user(
            email='test-user@test-user.ru',
            username='test-user',
            first_name='Тест',
            middle_name='Тестович',
            last_name='Юзер',
            password=self.password
        )
        self.app = Application.objects.create(
            client_id=settings.OAUTH2_CLIENT_ID,
            user=self.user,
            client_type=Application.CLIENT_PUBLIC,
            authorization_grant_type=Application.GRANT_PASSWORD,
            client_secret=settings.OAUTH2_CLIENT_SECRET,
            name=settings.OAUTH2_APP_NAME
        )

        self.lat = 20.936113
        self.lon = -32.197222

        # Авторизация
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.access_token = content["access_token"]

        # Авторизация для второго пользователя
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.target_user.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.target_user_access_token = content["access_token"]

        # Создание публикации
        self.publication = Publication.objects.create(
            user=self.target_user,
            data={'test': 'test'},
            lat=self.lat,
            lon=self.lon
        )

    def test_publication_vote_unauthorized(self):
        """
        Тест оценки публикации (Без авторизации)
        """
        r = self.client.post(reverse('publication_vote', kwargs={"pk": self.publication.pk}), data={'vote': Vote.LIKE})
        self.assertEquals(r.status_code, 401)

    def test_publication_vote_authorized(self):
        """
        Тест оценки публикации (С авторизацией)
        """
        r = self.client.post(reverse('publication_vote', kwargs={"pk": self.publication.pk}), data={'vote': Vote.LIKE},
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        vote = Vote.objects.get(
            user=self.user,
            content_type=ContentType.objects.get_for_model(self.publication),
            object_id=self.publication.pk
        )
        self.assertEquals(vote.vote, Vote.LIKE)

        # Проверка того, что сериализатор публикации возвращает оценки
        self.publication.refresh_from_db()
        serializer = PublicationSerializer(self.publication)
        self.assertEquals(serializer.data['likes_count'], 1)
        self.assertEquals(serializer.data['dislikes_count'], 0)

        # Попытка поставить лайк уже оцененной лайком публикации
        r = self.client.post(reverse('publication_vote', kwargs={"pk": self.publication.pk}), data={'vote': Vote.LIKE},
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        vote.refresh_from_db()
        self.assertEquals(vote.vote, Vote.LIKE)

        # Изменение оценки публикации на дизлайк
        r = self.client.post(reverse('publication_vote', kwargs={"pk": self.publication.pk}),
                             data={'vote': Vote.DISLIKE},
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        vote.refresh_from_db()
        self.assertEquals(vote.vote, Vote.DISLIKE)

        # Проверка того, что сериализатор публикации возвращает оценки
        self.publication.refresh_from_db()
        serializer = PublicationSerializer(self.publication)
        self.assertEquals(serializer.data['likes_count'], 0)
        self.assertEquals(serializer.data['dislikes_count'], 1)

        # Оценка публикации другим пользователем, счётчик дизлайков должен увеличиться
        r = self.client.post(reverse('publication_vote', kwargs={"pk": self.publication.pk}),
                             data={'vote': Vote.DISLIKE},
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.target_user_access_token))
        self.assertEquals(r.status_code, 200)
        self.publication.refresh_from_db()
        serializer = PublicationSerializer(self.publication)
        self.assertEquals(serializer.data['likes_count'], 0)
        self.assertEquals(serializer.data['dislikes_count'], 2)

    def test_comment_vote_unauthorized(self):
        """
        Тест оценки комментария (Без авторизации)
        """
        r = self.client.post(reverse('comment_vote', kwargs={"pk": self.publication.pk}), data={'vote': Vote.LIKE})
        self.assertEquals(r.status_code, 401)

    def test_comment_vote_authorized(self):
        """
        Тест оценки комментария (С авторизацией)
        """
        comment = Comment.objects.create(
            user=self.user,
            publication=self.publication,
            text='Текст комментария'
        )
        r = self.client.post(reverse('comment_vote', kwargs={"pk": comment.pk}), data={'vote': Vote.LIKE},
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        vote = Vote.objects.get(
            user=self.user,
            content_type=ContentType.objects.get_for_model(comment),
            object_id=comment.pk
        )
        self.assertEquals(vote.vote, Vote.LIKE)

        # Проверка того, что сериализатор публикации возвращает оценки
        comment.refresh_from_db()
        serializer = CommentSerializer(comment)
        self.assertEquals(serializer.data['likes_count'], 1)
        self.assertEquals(serializer.data['dislikes_count'], 0)

        # Попытка поставить лайк уже оцененной лайком публикации
        r = self.client.post(reverse('comment_vote', kwargs={"pk": comment.pk}), data={'vote': Vote.LIKE},
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        vote.refresh_from_db()
        self.assertEquals(vote.vote, Vote.LIKE)

        # Изменение оценки публикации на дизлайк
        r = self.client.post(reverse('comment_vote', kwargs={"pk": comment.pk}),
                             data={'vote': Vote.DISLIKE},
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        vote.refresh_from_db()
        self.assertEquals(vote.vote, Vote.DISLIKE)

        # Проверка того, что сериализатор публикации возвращает оценки
        comment.refresh_from_db()
        serializer = CommentSerializer(comment)
        self.assertEquals(serializer.data['likes_count'], 0)
        self.assertEquals(serializer.data['dislikes_count'], 1)

        # Оценка публикации другим пользователем, счётчик дизлайков должен увеличиться
        r = self.client.post(reverse('comment_vote', kwargs={"pk": comment.pk}),
                             data={'vote': Vote.DISLIKE},
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.target_user_access_token))
        self.assertEquals(r.status_code, 200)
        comment.refresh_from_db()
        serializer = CommentSerializer(comment)
        self.assertEquals(serializer.data['likes_count'], 0)
        self.assertEquals(serializer.data['dislikes_count'], 2)
