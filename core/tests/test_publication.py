import json

from django.conf import settings
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone
from oauth2_provider.models import Application

from core.models import User, Publication
from core.tests.utils import get_basic_auth_header


class PublicationTests(TestCase):
    """
    Тест методов получения, изменения профиля пользователя
    """
    def setUp(self):
        # Инициализация клиента
        self.host = 'localhost'
        self.client = Client(HTTP_HOST=self.host)

        self.email = 'arvarohfx@gmail.com'
        self.password = 'Str0ngPa$$w0rd'
        self.username = 'arvaroh'
        self.phone = '+79010003300'
        self.first_name = 'Дмитрий'
        self.middle_name = 'Сергеевич'
        self.last_name = 'Зотов'
        self.gender = 'MALE'
        self.birthday = timezone.now().date()

        self.user = User.objects.create_user(
            email=self.email,
            username=self.username,
            first_name=self.first_name,
            middle_name=self.middle_name,
            last_name=self.last_name,
            phone=self.phone,
            gender=self.gender,
            birthday=self.birthday,
            password=self.password
        )
        self.app = Application.objects.create(
            client_id=settings.OAUTH2_CLIENT_ID,
            user=self.user,
            client_type=Application.CLIENT_PUBLIC,
            authorization_grant_type=Application.GRANT_PASSWORD,
            client_secret=settings.OAUTH2_CLIENT_SECRET,
            name=settings.OAUTH2_APP_NAME
        )

        self.lat = 20.936113
        self.lon = -32.197222

        # Авторизация
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.access_token = content["access_token"]

    def test_publication_create_unauthorized(self):
        """
        Тест создания публикации (Без авторизации)
        """
        r = self.client.post(reverse('publication'),
                             data={"data": json.dumps({'test': 'test'}),
                                   "lat": self.lat,
                                   "lon": self.lon})
        self.assertEquals(r.status_code, 401)

    def test_publication_create_authorized(self):
        """
        Тест создания публикации (С авторизацией)
        """
        data = {'test': 'test'}
        publication_data = {
            "data": json.dumps({'test': 'test'}),
            "lat": self.lat,
            "lon": self.lon
        }
        r = self.client.post(reverse('publication'), data=publication_data,
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 201)
        content = r.json()
        publication = Publication.objects.get(pk=content['id'])
        self.assertEquals(publication.data, data)
        self.assertEquals(float(publication.lat), publication_data['lat'])
        self.assertEquals(float(publication.lon), publication_data['lon'])

    def test_get_publication_list_unauthorized(self):
        """
        Получить список публикаций (Без авторизации)
        """
        r = self.client.get(reverse('publication'))
        self.assertEquals(r.status_code, 401)

    def test_get_publication_list_authorized(self):
        """
        Получить список публикаций (С авторизацией)
        """
        publication = Publication.objects.create(
            user=self.user,
            data=json.dumps({'test': 'test'}),
            lat=self.lat,
            lon=self.lon
        )

        # Получить список всех публикаций
        r = self.client.get(reverse('publication'), HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = r.json()
        self.assertIsInstance(content, list)
        self.assertEquals(len(content), 1)
        self.assertEquals(publication.data, content[0]['data'])
        self.assertEquals(str(publication.lat), content[0]['lat'])
        self.assertEquals(str(publication.lon), content[0]['lon'])

    def test_get_publication_detail_unauthorized(self):
        """
        Получить подробную информацию по публикации (Без авторизации)
        """
        publication = Publication.objects.create(
            user=self.user,
            data=json.dumps({'test': 'test'}),
            lat=self.lat,
            lon=self.lon
        )
        r = self.client.get(reverse('publication', kwargs={"pk": publication.pk}))
        self.assertEquals(r.status_code, 401)

    def test_get_publication_detail_authorized(self):
        """
        Получить подробную информацию по публикации (С авторизацией)
        """
        publication = Publication.objects.create(
            user=self.user,
            data=json.dumps({'test': 'test'}),
            lat=self.lat,
            lon=self.lon
        )

        # Получить список публикацию по идентификатору
        r = self.client.get(reverse('publication', kwargs={"pk": publication.pk}),
                            HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = r.json()
        self.assertEquals(publication.data, content['data'])
        self.assertEquals(str(publication.lat), content['lat'])
        self.assertEquals(str(publication.lon), content['lon'])

    def test_delete_publication_unauthorized(self):
        """
        Удалить публикацию (Без авторизации)
        """
        publication = Publication.objects.create(
            user=self.user,
            data=json.dumps({'test': 'test'}),
            lat=self.lat,
            lon=self.lon
        )
        r = self.client.delete(reverse('publication', kwargs={'pk': publication.pk}))
        self.assertEquals(r.status_code, 401)

    def test_delete_publication_authorized(self):
        """
        Удалить публикацию (C авторизацией)
        """
        publication = Publication.objects.create(
            user=self.user,
            data=json.dumps({'test': 'test'}),
            lat=self.lat,
            lon=self.lon
        )
        r = self.client.delete(reverse('publication', kwargs={'pk': publication.pk}),
                               HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 204)
        publication.refresh_from_db()
        self.assertTrue(publication.is_deleted)

    def test_update_publication_unauthorized(self):
        """
        Полное обновление публикации (Без авторизации)
        """
        publication = Publication.objects.create(
            user=self.user,
            data=json.dumps({'test': 'test'}),
            lat=self.lat,
            lon=self.lon
        )
        new_data = {
            'data': json.dumps({'new': 'data'}),
            'lat': 22.222222,
            'lon': 33.333333
        }
        r = self.client.put(reverse('publication', kwargs={'pk': publication.pk}), data=new_data,
                            content_type='application/json',)
        self.assertEquals(r.status_code, 401)

    def test_update_publication_authorized(self):
        """
        Полное обновление публикации (С авторизацией)
        """
        publication = Publication.objects.create(
            user=self.user,
            data=json.dumps({'test': 'test'}),
            lat=self.lat,
            lon=self.lon
        )
        new_data = {
            'data': json.dumps({'new': 'data'}),
            'lat': 22.222222,
            'lon': 33.333333
        }
        r = self.client.put(reverse('publication', kwargs={'pk': publication.pk}), data=new_data,
                            HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token),
                            content_type='application/json',)
        self.assertEquals(r.status_code, 200)
        publication.refresh_from_db()
        self.assertEquals(publication.data, new_data['data'])
        self.assertEquals(float(publication.lat), new_data['lat'])
        self.assertEquals(float(publication.lon), new_data['lon'])

    def test_partial_update_publication_unauthorized(self):
        """
        Частичное обновление публикации (Без авторизации)
        """
        publication = Publication.objects.create(
            user=self.user,
            data=json.dumps({'test': 'test'}),
            lat=self.lat,
            lon=self.lon
        )
        new_data = {
            'data': json.dumps({'new': 'data'}),
            'lat': 22.222222,
            'lon': 33.333333
        }
        r = self.client.patch(reverse('publication', kwargs={'pk': publication.pk}), data=new_data,
                              content_type='application/json',)
        self.assertEquals(r.status_code, 401)

    def test_partial_update_publication_authorized(self):
        """
        Частичное обновление публикации (С авторизацией)
        """
        publication = Publication.objects.create(
            user=self.user,
            data=json.dumps({'test': 'test'}),
            lat=self.lat,
            lon=self.lon
        )
        new_data = {
            'data': json.dumps({'new': 'data'}),
            'lat': 22.222222,
            'lon': 33.333333
        }
        r = self.client.patch(reverse('publication', kwargs={'pk': publication.pk}), data=new_data,
                              HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token),
                              content_type='application/json')
        self.assertEquals(r.status_code, 200)
        publication.refresh_from_db()
        self.assertEquals(publication.data, new_data['data'])
        self.assertEquals(float(publication.lat), new_data['lat'])
        self.assertEquals(float(publication.lon), new_data['lon'])
