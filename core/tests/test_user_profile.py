import io
import json

from django.conf import settings
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone
from oauth2_provider.models import Application

from core.models import User
from core.models.common import VerificationCode
from core.tests.utils import get_basic_auth_header


class UserProfileTests(TestCase):
    """
    Тест методов получения, изменения профиля пользователя
    """
    def setUp(self):
        # Инициализация клиента
        self.host = 'localhost'
        self.client = Client(HTTP_HOST=self.host)

        self.email = 'arvarohfx@gmail.com'
        self.password = 'Str0ngPa$$w0rd'
        self.username = 'arvaroh'
        self.phone = '+79010003300'
        self.first_name = 'Дмитрий'
        self.middle_name = 'Сергеевич'
        self.last_name = 'Зотов'
        self.gender = 'MALE'
        self.birthday = timezone.now().date()
        self.avatar = (io.BytesIO(b"abcdef"), 'test.jpg')

        self.user = User.objects.create_user(
            email=self.email,
            username=self.username,
            first_name=self.first_name,
            middle_name=self.middle_name,
            last_name=self.last_name,
            phone=self.phone,
            gender=self.gender,
            birthday=self.birthday,
            password=self.password
        )
        self.app = Application.objects.create(
            client_id=settings.OAUTH2_CLIENT_ID,
            user=self.user,
            client_type=Application.CLIENT_PUBLIC,
            authorization_grant_type=Application.GRANT_PASSWORD,
            client_secret=settings.OAUTH2_CLIENT_SECRET,
            name=settings.OAUTH2_APP_NAME
        )

        # Авторизация
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.access_token = content["access_token"]

    def test_get_profile_unauthorized(self):
        """
        Получить профиль неавторизованным пользователем
        """
        r = self.client.get(reverse('user_profile', kwargs={'pk': self.user.pk}))
        self.assertEquals(r.status_code, 401)

    def test_get_profile_authorized(self):
        """
        Получить профиль пользователя ным пользователем
        """
        r = self.client.get(reverse('user_profile', kwargs={'pk': self.user.pk}),
                            HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = r.json()
        self.assertEquals(content['id'], self.user.pk)
        self.assertEquals(content['email'], self.user.email)
        self.assertEquals(content['username'], self.user.username)
        self.assertEquals(content['phone'], self.phone)
        self.assertEquals(content['first_name'], self.user.first_name)
        self.assertEquals(content['middle_name'], self.user.middle_name)
        self.assertEquals(content['last_name'], self.user.last_name)
        self.assertEquals(content['gender'], self.gender)
        self.assertEquals(content['birthday'], str(self.birthday))

    def test_profile_update_unauthorized(self):
        """
        Полное обновление профиля пользователя (PUT) неавторизованным пользователем
        """
        r = self.client.put(reverse('user_profile', kwargs={'pk': self.user.pk}))
        self.assertEquals(r.status_code, 401)

    # def test_profile_update_authorized(self):
    #     """
    #     Полное обновление профиля пользователя (PUT) авторизованным пользователем
    #     """
    #     data = {
    #         "email": self.email,
    #         "username": self.username,
    #         "password": self.password,
    #         "phone": self.phone,
    #         "first_name": self.first_name,
    #         "middle_name": self.middle_name,
    #         "last_name": self.last_name,
    #         "gender": self.gender,
    #         "birthday": self.birthday,
    #         "avatar": self.avatar
    #     }
    #     r = self.client.put(reverse('user_profile', kwargs={'pk': self.user.pk}),
    #                         data=data,
    #                         content_type='multipart/form-data',
    #                         HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
    #     self.assertEquals(r.status_code, 200)

    def test_profile_partial_update_unauthorized(self):
        """
        Частичное обновление профиля пользователя (PATCH) неавторизованным пользователем
        """
        r = self.client.patch(reverse('user_profile', kwargs={'pk': self.user.pk}))
        self.assertEquals(r.status_code, 401)

    # def test_profile_partial_update_authorized(self):
    #     """
    #     Частичное обновление профиля пользователя (PATCH) авторизованным пользователем
    #     """
    #     new_email = 'test@test.ru'
    #     data = {
    #         "email": new_email,
    #         "avatar": self.avatar
    #     }
    #     r = self.client.patch(reverse('user_profile', kwargs={'pk': self.user.pk}),
    #                           data=data,
    #                           content_type='multipart/form-data',
    #                           HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
    #     self.assertEquals(r.status_code, 200)
    #     self.user.refresh_from_db()

    def test_password_reset(self):
        """
        Тест методов для восстановления пароля
        """
        r = self.client.post(reverse('user_password_reset'), data={'email': self.email})
        self.assertEquals(r.status_code, 200)

        # Проверка того, что код верификации электронного адреса был создан
        verification_code = VerificationCode.objects.get(
            email=self.email, code_type=VerificationCode.CODES.VERIFICATION_EMAIL_CODE)
        self.assertIsNotNone(verification_code)

        # Верификация электронного адреса
        r = self.client.post(reverse('user_email_confirmation'),
                             data={'verification_code': verification_code.verification_code})
        self.assertEquals(r.status_code, 200)
        hash_uuid = r.json().get('hash_uuid')
        self.assertIsNotNone(hash_uuid)

        # Задание нового пароля
        new_password = 'NewStr0ngPa$$w0rd'
        r = self.client.patch(reverse('user_password_reset'),
                              data={'hash_uuid': hash_uuid, 'new_password': new_password},
                              content_type='application/json',
                              HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)

        # Попытка авторизации с новым паролем
        # Авторизация
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.email,
            "password": new_password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.access_token = content["access_token"]
