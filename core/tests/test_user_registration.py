import json

from django.conf import settings
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone
from oauth2_provider.models import Application

from core.models import User
from core.models.common import VerificationCode
from core.tests.utils import get_basic_auth_header


class UserRegistrationTests(TestCase):
    """
    Тесты регистрации пользователя
    """
    def setUp(self):
        # Инициализация клиента
        self.host = 'localhost'
        self.client = Client(HTTP_HOST=self.host)

        self.email = 'arvarohfx@gmail.com'
        self.password = 'Str0ngPa$$w0rd'
        self.username = 'arvaroh'
        self.phone = '+79010003300'
        self.first_name = 'Дмитрий'
        self.middle_name = 'Сергеевич'
        self.last_name = 'Зотов'
        self.gender = 'MALE'
        self.birthday = timezone.now().date()

    def tearDown(self):
        pass

    def test_registration(self):
        """
        Тест регистрации пользователя и последующей аутентификации
        """
        data = {
            "email": self.email,
            "username": self.username,
            "password": self.password,
            "phone": self.phone,
            "first_name": self.first_name,
            "middle_name": self.middle_name,
            "last_name": self.last_name,
            "gender": self.gender,
            "birthday": self.birthday
        }
        r = self.client.post(reverse('user_registration'), data=data)
        self.assertEqual(r.status_code, 201)

        user = User.objects.get(email=self.email)
        self.assertEqual(user.username, self.username)
        # TODO: self.assertContains(user.phone, self.phone)
        self.assertEqual(user.first_name, self.first_name)
        self.assertEqual(user.middle_name, self.middle_name)
        self.assertEqual(user.last_name, self.last_name)
        self.assertEqual(user.gender, self.gender)
        self.assertEqual(user.birthday, self.birthday)
        self.assertFalse(user.is_active)

        # Проверка того, что код верификации электронного адреса был создан
        verification_code = VerificationCode.objects.get(
            email=self.email, code_type=VerificationCode.CODES.VERIFICATION_EMAIL_CODE)
        self.assertIsNotNone(verification_code)

        # Активируем пользователя
        r = self.client.post(reverse('user_email_confirmation'),
                             data={'verification_code': verification_code.verification_code})
        self.assertEquals(r.status_code, 200)

        user.refresh_from_db()
        self.assertTrue(user.is_active)
        verification_code.refresh_from_db()
        self.assertIsNotNone(verification_code.validated)

        # Тест авторизации только что созданного пользователя
        self.app = Application.objects.create(
            client_id=settings.OAUTH2_CLIENT_ID,
            user=user,
            client_type=Application.CLIENT_PUBLIC,
            authorization_grant_type=Application.GRANT_PASSWORD,
            client_secret=settings.OAUTH2_CLIENT_SECRET,
            name=settings.OAUTH2_APP_NAME
        )

        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        self.assertEqual(r.status_code, 200)

        content = json.loads(r.content.decode("utf-8"))
        self.assertEqual(content["token_type"], "Bearer")
        self.assertEqual(content["scope"], "read write groups")
        self.assertIsNotNone(content["access_token"])
        self.assertIsNotNone(content["refresh_token"])
