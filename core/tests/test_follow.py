import json

from django.conf import settings
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone
from oauth2_provider.models import Application

from core.models import User
from core.models.follow import Follow
from core.tests.utils import get_basic_auth_header


class FriendRequestTests(TestCase):
    """
    Тест методов получения и изменения заявки в друзья
    """
    def setUp(self):
        # Инициализация клиента
        self.host = 'localhost'
        self.client = Client(HTTP_HOST=self.host)

        self.email = 'arvarohfx@gmail.com'
        self.password = 'Str0ngPa$$w0rd'
        self.username = 'arvaroh'
        self.phone = '+79010003300'
        self.first_name = 'Дмитрий'
        self.middle_name = 'Сергеевич'
        self.last_name = 'Зотов'
        self.gender = 'MALE'
        self.birthday = timezone.now().date()

        self.user = User.objects.create_user(
            email=self.email,
            username=self.username,
            first_name=self.first_name,
            middle_name=self.middle_name,
            last_name=self.last_name,
            phone=self.phone,
            gender=self.gender,
            birthday=self.birthday,
            password=self.password
        )

        self.target_user = User.objects.create_user(
            email='test-user@test-user.ru',
            username='test-user',
            first_name='Тест',
            middle_name='Тестович',
            last_name='Юзер',
            password=self.password
        )
        self.app = Application.objects.create(
            client_id=settings.OAUTH2_CLIENT_ID,
            user=self.user,
            client_type=Application.CLIENT_PUBLIC,
            authorization_grant_type=Application.GRANT_PASSWORD,
            client_secret=settings.OAUTH2_CLIENT_SECRET,
            name=settings.OAUTH2_APP_NAME
        )

        # Авторизация
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.access_token = content["access_token"]

        # Авторизация для второго пользователя
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.target_user.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.target_user_access_token = content["access_token"]

    def test_follow_create_unauthorized(self):
        """
        Тест подписки на пользователя (без авторизации)
        """
        r = self.client.post(reverse("follower"), data={"target_user_id": self.target_user.pk})
        self.assertEquals(r.status_code, 401)

    def test_follow_create(self):
        """
        Тест подписки на пользователя
        """
        r = self.client.post(reverse("follower"), data={"target_user_id": self.target_user.pk},
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 201)

        follow = Follow.objects.get(follower=self.user, target=self.target_user)
        self.assertIsNotNone(follow)

    def test_get_followers_and_subscriptions_unauthorized(self):
        """
        Получить список всех подписок и подписчиков (без авторизации)
        """
        r = self.client.get(reverse("follower"))
        self.assertEquals(r.status_code, 401)

    def test_get_followers_and_subscriptions(self):
        """
        Тест подписки на пользователя
        """
        # Подписаться на пользователя
        r = self.client.post(reverse("follower"), data={"target_user_id": self.target_user.pk},
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 201)

        r = self.client.get(reverse("follower"), HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = json.loads(r.content.decode("utf-8"))
        self.assertEquals(len(content['followers']), 0)
        self.assertEquals(len(content['subscriptions']), 1)

        # Подписаться в ответ
        r = self.client.post(reverse("follower"), data={"target_user_id": self.user.pk},
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.target_user_access_token))
        self.assertEquals(r.status_code, 201)

        r = self.client.get(reverse("follower"), HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = json.loads(r.content.decode("utf-8"))
        self.assertEquals(len(content['followers']), 1)
        self.assertEquals(len(content['subscriptions']), 1)

    def test_unsubscribe_unauthorized(self):
        """
        Тест метода отписки от пользователя (без авторизации)
        """
        r = self.client.delete(reverse("follower", kwargs={'user_id': self.target_user.pk}))
        self.assertEquals(r.status_code, 401)

    def test_unsubscribe(self):
        """
        Тест метода отписки от пользователя
        """
        # Подписаться на пользователя
        r = self.client.post(reverse("follower"), data={"target_user_id": self.target_user.pk},
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 201)

        # Проверка подписки
        r = self.client.get(reverse("follower"), HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = json.loads(r.content.decode("utf-8"))
        self.assertEquals(len(content['followers']), 0)
        self.assertEquals(len(content['subscriptions']), 1)

        # Отписка
        r = self.client.delete(reverse("follower", kwargs={'user_id': self.target_user.pk}),
                               HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 204)

        # Проверка отписки
        r = self.client.get(reverse("follower"), HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = json.loads(r.content.decode("utf-8"))
        self.assertEquals(len(content['followers']), 0)
        self.assertEquals(len(content['subscriptions']), 0)

        # Отписка (если её нет)
        r = self.client.delete(reverse("follower", kwargs={'user_id': self.target_user.pk}),
                               HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 404)
