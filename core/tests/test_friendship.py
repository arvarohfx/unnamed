import json

from django.conf import settings
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone
from oauth2_provider.models import Application

from core.models import User
from core.models.friendship import Friendship
from core.tests.utils import get_basic_auth_header


class FriendRequestTests(TestCase):
    """
    Тест методов получения и изменения заявки в друзья
    """
    def setUp(self):
        # Инициализация клиента
        self.host = 'localhost'
        self.client = Client(HTTP_HOST=self.host)

        self.email = 'arvarohfx@gmail.com'
        self.password = 'Str0ngPa$$w0rd'
        self.username = 'arvaroh'
        self.phone = '+79010003300'
        self.first_name = 'Дмитрий'
        self.middle_name = 'Сергеевич'
        self.last_name = 'Зотов'
        self.gender = 'MALE'
        self.birthday = timezone.now().date()

        self.user = User.objects.create_user(
            email=self.email,
            username=self.username,
            first_name=self.first_name,
            middle_name=self.middle_name,
            last_name=self.last_name,
            phone=self.phone,
            gender=self.gender,
            birthday=self.birthday,
            password=self.password
        )

        self.target_user = User.objects.create_user(
            email='test-user@test-user.ru',
            username='test-user',
            first_name='Тест',
            middle_name='Тестович',
            last_name='Юзер',
            password=self.password
        )
        self.app = Application.objects.create(
            client_id=settings.OAUTH2_CLIENT_ID,
            user=self.user,
            client_type=Application.CLIENT_PUBLIC,
            authorization_grant_type=Application.GRANT_PASSWORD,
            client_secret=settings.OAUTH2_CLIENT_SECRET,
            name=settings.OAUTH2_APP_NAME
        )

        self.lat = 20.936113
        self.lon = -32.197222

        # Авторизация
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.access_token = content["access_token"]

        # Авторизация для второго пользователя
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.target_user.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.target_user_access_token = content["access_token"]

    def test_get_friendship_list_unauthorized(self):
        """
        Получить список друзей (Без авторизации)
        """
        r = self.client.get(reverse('friendship'))
        self.assertEquals(r.status_code, 401)

    def test_get_friendship_list_authorized(self):
        """
        Получить список запросов в друзья (С авторизацией)
        """
        # Создание связи друзей
        Friendship.objects.befriend(self.user, self.target_user)

        # Получить список всех друзей от имени первого пользователя
        r = self.client.get(reverse('friendship'), HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = r.json()
        self.assertEquals(type(content), list)
        self.assertEquals(content[0]['id'], self.target_user.pk)

        # Получить список всех друзей от имени второго пользователя
        r = self.client.get(reverse('friendship'), HTTP_AUTHORIZATION='Bearer {}'.format(self.target_user_access_token))
        self.assertEquals(r.status_code, 200)
        content = r.json()
        self.assertEquals(type(content), list)
        self.assertEquals(content[0]['id'], self.user.pk)

    def test_remove_from_friends_unauthorized(self):
        """
        Удалить из друзей (Без авторизации)
        """
        # Создание связи друзей
        Friendship.objects.befriend(self.user, self.target_user)

        r = self.client.delete(reverse('friendship_request', kwargs={'pk': self.target_user.pk}))
        self.assertEquals(r.status_code, 401)

    def test_remove_from_friends_authorized(self):
        """
        Удалить из друзей (С авторизацией)
        """
        # Создание связи друзей
        Friendship.objects.befriend(self.user, self.target_user)

        # Проверка, что пользователи теперь друзья
        r = self.client.get(reverse('friendship'), HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = r.json()
        self.assertEquals(len(content), 1)

        r = self.client.delete(reverse('friendship', kwargs={'user_id': self.target_user.pk}),
                               HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 204)

        # Проверка, что удалённый друг больше не возвращается в списке друзей
        r = self.client.get(reverse('friendship'), HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = r.json()
        self.assertEquals(len(content), 0)

        # Удалить из друзей пользователя, который не является другом
        r = self.client.delete(reverse('friendship', kwargs={'user_id': self.user.pk}),
                               HTTP_AUTHORIZATION='Bearer {}'.format(self.target_user_access_token))
        self.assertEquals(r.status_code, 403)
