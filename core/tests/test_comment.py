import json
from datetime import timedelta

from django.conf import settings
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone
from oauth2_provider.models import Application

from core.models import User, Publication
from core.models.comment import Comment
from core.tests.utils import get_basic_auth_header


class CommentTests(TestCase):
    """
    Тест методов создания, получения, изменения и удаления комментария к публикации
    """
    def setUp(self):
        # Инициализация клиента
        self.host = 'localhost'
        self.client = Client(HTTP_HOST=self.host)

        self.email = 'arvarohfx@gmail.com'
        self.password = 'Str0ngPa$$w0rd'
        self.username = 'arvaroh'
        self.phone = '+79010003300'
        self.first_name = 'Дмитрий'
        self.middle_name = 'Сергеевич'
        self.last_name = 'Зотов'
        self.gender = 'MALE'
        self.birthday = timezone.now().date()

        self.user = User.objects.create_user(
            email=self.email,
            username=self.username,
            first_name=self.first_name,
            middle_name=self.middle_name,
            last_name=self.last_name,
            phone=self.phone,
            gender=self.gender,
            birthday=self.birthday,
            password=self.password
        )

        self.target_user = User.objects.create_user(
            email='test-user@test-user.ru',
            username='test-user',
            first_name='Тест',
            middle_name='Тестович',
            last_name='Юзер',
            password=self.password
        )
        self.app = Application.objects.create(
            client_id=settings.OAUTH2_CLIENT_ID,
            user=self.user,
            client_type=Application.CLIENT_PUBLIC,
            authorization_grant_type=Application.GRANT_PASSWORD,
            client_secret=settings.OAUTH2_CLIENT_SECRET,
            name=settings.OAUTH2_APP_NAME
        )

        self.lat = 20.936113
        self.lon = -32.197222

        # Авторизация
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.access_token = content["access_token"]

        # Авторизация для второго пользователя
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.target_user.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.target_user_access_token = content["access_token"]

        # Создание публикации
        self.publication = Publication.objects.create(
            user=self.target_user,
            data={'test': 'test'},
            lat=self.lat,
            lon=self.lon
        )

    def test_comment_create_unauthorized(self):
        """
        Тест создания комментария к публикации (Без авторизации)
        """
        data = {
            'publication_id': self.publication.pk,
            'text': 'Комментарий'
        }
        r = self.client.post(reverse('comment'), data=data)
        self.assertEquals(r.status_code, 401)

    def test_comment_create_authorized(self):
        """
        Тест создания комментария к публикации (С авторизацией)
        """
        data = {
            'publication_id': self.publication.pk,
            'text': 'Комментарий'
        }
        r = self.client.post(reverse('comment'), data=data,
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 201)
        content = r.json()
        comment = Comment.objects.get(pk=content['id'])
        self.assertEquals(comment.user, self.user)
        self.assertEquals(comment.publication, self.publication)
        self.assertEquals(comment.text, data['text'])
        self.assertFalse(comment.is_deleted)

        # Попытка создания много комментариев за короткий промежуток, первые 10 комментариев создадуться,
        # но 11й - создать уже нельзя, достигнут лимит по комментариям в единицу времени
        for i in range(11):
            r = self.client.post(reverse('comment'), data=data,
                                 HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 429)

    def test_get_comment_list_unauthorized(self):
        """
        Получить список комментариев к публикации (Без авторизации)
        """
        r = self.client.get(reverse('comment'))
        self.assertEquals(r.status_code, 401)

    def test_get_comment_list_authorized(self):
        """
        Получить список комментариев к публикации (С авторизацией)
        """
        # Создать комментарий
        comment = Comment.objects.create(
            user=self.user,
            publication=self.publication,
            text='Текст комментария'
        )

        data = {
            'publication_id': self.publication.pk
        }
        # Получить список всех входящих и исходящих заявок
        r = self.client.get(reverse('comment'), data=data, HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = r.json()
        self.assertIsInstance(content, list)
        self.assertEquals(len(content), 1)
        self.assertEquals(content[0]['id'], comment.pk)
        self.assertEquals(content[0]['text'], comment.text)

    def test_update_comment_unauthorized(self):
        """
        Изменить комментарий (Без авторизации)
        """
        # Создать комментарий
        comment = Comment.objects.create(
            user=self.user,
            publication=self.publication,
            text='Текст комментария'
        )
        r = self.client.patch(reverse('comment', kwargs={'pk': comment.pk}))
        self.assertEquals(r.status_code, 401)

    def test_update_comment_authorized(self):
        """
        Изменить комментарий (С авторизацией)
        """
        # Создать комментарий
        comment = Comment.objects.create(
            user=self.user,
            publication=self.publication,
            text='Текст комментария'
        )
        data = {'text': 'Новый текст комментария'}

        # Попытка изменить комментарий, не являясь его владельцем
        r = self.client.patch(reverse('comment', kwargs={'pk': comment.pk}),
                              data=data,
                              content_type='application/json',
                              HTTP_AUTHORIZATION='Bearer {}'.format(self.target_user_access_token))
        self.assertEquals(r.status_code, 403)

        r = self.client.patch(reverse('comment', kwargs={'pk': comment.pk}),
                              data=data,
                              content_type='application/json',
                              HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        comment.refresh_from_db()
        self.assertEquals(comment.text, data['text'])
        self.assertTrue(comment.is_edited)

        # Попытка отредактировать комментарий, если прошло 30 минут с момента публикации
        comment.created = timezone.now() - timedelta(minutes=30)
        comment.save()
        r = self.client.patch(reverse('comment', kwargs={'pk': comment.pk}),
                              data=data,
                              content_type='application/json',
                              HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 403)

    def test_delete_comment_unauthorized(self):
        """
        Удалить комментарий (Без авторизации)
        """
        # Создать комментарий
        comment = Comment.objects.create(
            user=self.user,
            publication=self.publication,
            text='Текст комментария'
        )
        r = self.client.delete(reverse('comment', kwargs={'pk': comment.pk}))
        self.assertEquals(r.status_code, 401)

    def test_delete_comment_authorized(self):
        """
        Удалить комментарий (С авторизацией)
        """
        # Создать комментарий
        comment = Comment.objects.create(
            user=self.user,
            publication=self.publication,
            text='Текст комментария'
        )
        # Попытка удалить комментарий, не являясь его владельцем
        r = self.client.delete(reverse('comment', kwargs={'pk': comment.pk}),
                               HTTP_AUTHORIZATION='Bearer {}'.format(self.target_user_access_token))
        self.assertEquals(r.status_code, 403)

        r = self.client.delete(reverse('comment', kwargs={'pk': comment.pk}),
                               HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 204)
        comment.refresh_from_db()
        self.assertTrue(comment.is_deleted)
