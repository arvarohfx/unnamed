import json

from django.conf import settings
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone
from oauth2_provider.models import Application

from core.models import User
from core.models.friendship import FriendshipRequest, Friendship
from core.tests.utils import get_basic_auth_header


class FriendRequestTests(TestCase):
    """
    Тест методов получения и изменения заявки в друзья
    """
    def setUp(self):
        # Инициализация клиента
        self.host = 'localhost'
        self.client = Client(HTTP_HOST=self.host)

        self.email = 'arvarohfx@gmail.com'
        self.password = 'Str0ngPa$$w0rd'
        self.username = 'arvaroh'
        self.phone = '+79010003300'
        self.first_name = 'Дмитрий'
        self.middle_name = 'Сергеевич'
        self.last_name = 'Зотов'
        self.gender = 'MALE'
        self.birthday = timezone.now().date()

        self.user = User.objects.create_user(
            email=self.email,
            username=self.username,
            first_name=self.first_name,
            middle_name=self.middle_name,
            last_name=self.last_name,
            phone=self.phone,
            gender=self.gender,
            birthday=self.birthday,
            password=self.password
        )

        self.target_user = User.objects.create_user(
            email='test-user@test-user.ru',
            username='test-user',
            first_name='Тест',
            middle_name='Тестович',
            last_name='Юзер',
            password=self.password
        )
        self.app = Application.objects.create(
            client_id=settings.OAUTH2_CLIENT_ID,
            user=self.user,
            client_type=Application.CLIENT_PUBLIC,
            authorization_grant_type=Application.GRANT_PASSWORD,
            client_secret=settings.OAUTH2_CLIENT_SECRET,
            name=settings.OAUTH2_APP_NAME
        )

        self.lat = 20.936113
        self.lon = -32.197222

        # Авторизация
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.access_token = content["access_token"]

        # Авторизация для второго пользователя
        token_request_data = {
            "grant_type": self.app.authorization_grant_type,
            "username": self.target_user.email,
            "password": self.password,
        }
        auth_headers = get_basic_auth_header(self.app.client_id, self.app.client_secret)

        r = self.client.post(reverse("oauth2_provider:token"), data=token_request_data, **auth_headers)
        content = json.loads(r.content.decode("utf-8"))
        self.target_user_access_token = content["access_token"]

    def test_friendship_request_create_unauthorized(self):
        """
        Тест создания запроса в друзья (Без авторизации)
        """
        r = self.client.post(reverse('friendship_request'), data={"target_user_id": self.target_user.pk})
        self.assertEquals(r.status_code, 401)

    def test_friendship_request_create_authorized(self):
        """
        Тест создания запроса в друзья (С авторизацией)
        """
        friendship_request_data = {"target_user_id": self.target_user.pk}
        r = self.client.post(reverse('friendship_request'), data=friendship_request_data,
                             HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 201)
        content = r.json()
        friendship_request = FriendshipRequest.objects.get(pk=content['id'])
        self.assertEquals(friendship_request.from_user, self.user)
        self.assertEquals(friendship_request.to_user, self.target_user)
        self.assertFalse(friendship_request.is_accepted)

    def test_get_friendship_request_list_unauthorized(self):
        """
        Получить список запросов в друзья (Без авторизации)
        """
        r = self.client.get(reverse('friendship_request'))
        self.assertEquals(r.status_code, 401)

    def test_get_friendship_request_list_authorized(self):
        """
        Получить список запросов в друзья (С авторизацией)
        """
        outgoing_friendship_request = FriendshipRequest.objects.create(from_user=self.user, to_user=self.target_user)
        incoming_friendship_request = FriendshipRequest.objects.create(from_user=self.target_user, to_user=self.user)

        # Получить список всех входящих и исходящих заявок
        r = self.client.get(reverse('friendship_request'), HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = r.json()
        self.assertIsInstance(content['outgoing'], list)
        self.assertIsInstance(content['incoming'], list)
        self.assertEquals(len(content['outgoing']), 1)
        self.assertEquals(len(content['incoming']), 1)

        # Принять все заявки
        outgoing_friendship_request.is_accepted = True
        outgoing_friendship_request.save()
        incoming_friendship_request.is_accepted = True
        incoming_friendship_request.save()

        # Принятые заявки не должны возвращатся в ответе
        r = self.client.get(reverse('friendship_request'), HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = r.json()
        self.assertEquals(len(content['outgoing']), 0)
        self.assertEquals(len(content['incoming']), 0)

    def test_get_friendship_request_detail_unauthorized(self):
        """
        Получить подробную информацию по запросу в друзья (Без авторизации)
        """
        friendship_request = FriendshipRequest.objects.create(from_user=self.user, to_user=self.target_user)
        r = self.client.get(reverse('friendship_request', kwargs={"pk": friendship_request.pk}))
        self.assertEquals(r.status_code, 401)

    def test_get_friendship_request_detail_authorized(self):
        """
        Получить подробную информацию по запросу в друзья (С авторизацией)
        """
        friendship_request = FriendshipRequest.objects.create(from_user=self.user, to_user=self.target_user)

        # Получить список запрос в друзья по идентификатору
        r = self.client.get(reverse('friendship_request', kwargs={"pk": friendship_request.pk}),
                            HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 200)
        content = r.json()
        self.assertEquals(friendship_request.pk, content['id'])
        self.assertEquals(friendship_request.to_user.pk, content['to_user']['id']),
        self.assertEquals(friendship_request.is_accepted, content['is_accepted'])

    def test_delete_friendship_request_unauthorized(self):
        """
        Удалить (отменить/отклонить) запрос в друзья (Без авторизации)
        """
        friendship_request = FriendshipRequest.objects.create(from_user=self.user, to_user=self.target_user)
        r = self.client.delete(reverse('friendship_request', kwargs={'pk': friendship_request.pk}))
        self.assertEquals(r.status_code, 401)

    def test_delete_friendship_request_authorized(self):
        """
        Удалить (отменить/отклонить) запрос в друзья (C авторизацией)
        """
        # Отмена исходящего запроса
        outgoing_friendship_request = FriendshipRequest.objects.create(from_user=self.user, to_user=self.target_user)
        r = self.client.delete(reverse('friendship_request', kwargs={'pk': outgoing_friendship_request.pk}),
                               HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 204)
        self.assertRaises(FriendshipRequest.DoesNotExist, outgoing_friendship_request.refresh_from_db)

        # Отклонение входящего запроса
        incoming_friendship_request = FriendshipRequest.objects.create(from_user=self.target_user, to_user=self.user)
        r = self.client.delete(reverse('friendship_request', kwargs={'pk': incoming_friendship_request.pk}),
                               HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token))
        self.assertEquals(r.status_code, 204)
        self.assertRaises(FriendshipRequest.DoesNotExist, incoming_friendship_request.refresh_from_db)

    def test_partial_update_friendship_request_unauthorized(self):
        """
        Частичное обновление запроса в друзья - принять запрос (Без авторизации)
        """
        friendship_request = FriendshipRequest.objects.create(from_user=self.user, to_user=self.target_user)
        r = self.client.patch(reverse('friendship_request', kwargs={'pk': friendship_request.pk}),
                              content_type='application/json',)
        self.assertEquals(r.status_code, 401)

    def test_partial_update_friendship_request_authorized(self):
        """
        Частичное обновление запроса в друзья - принять запрос (С авторизацией)
        """
        friendship_request = FriendshipRequest.objects.create(from_user=self.user, to_user=self.target_user)

        # Попытка подтвердить запрос, не являясь при этом целью запроса
        r = self.client.patch(reverse('friendship_request', kwargs={'pk': friendship_request.pk}),
                              HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token),
                              content_type='application/json')
        self.assertEquals(r.status_code, 403)

        incoming_friendship_request = FriendshipRequest.objects.create(from_user=self.target_user, to_user=self.user)
        r = self.client.patch(reverse('friendship_request', kwargs={'pk': incoming_friendship_request.pk}),
                              HTTP_AUTHORIZATION='Bearer {}'.format(self.access_token),
                              content_type='application/json')
        self.assertEquals(r.status_code, 200)
        incoming_friendship_request.refresh_from_db()
        self.assertTrue(incoming_friendship_request.is_accepted)
        # Проверка, появился ли пользователь в списке друзей
        self.assertTrue(Friendship.objects.are_friends(self.user, self.target_user))
        self.assertTrue(Friendship.objects.are_friends(self.target_user, self.user))
