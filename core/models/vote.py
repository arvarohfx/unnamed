from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

from core.models.common import TimestampedAbstractModel


class VoteManager(models.Manager):
    use_for_related_fields = True

    def likes(self):
        """
        Получить все записи, у которых оценка больше 0
        """
        return self.get_queryset().filter(vote=Vote.LIKE)

    def dislikes(self):
        """
        Получить все записи, у которых оценка меньше 0
        """
        return self.get_queryset().filter(vote=Vote.DISLIKE)

    def sum_rating(self):
        """
        Получить общий рейтинг
        """
        return self.likes_count() - self.dislikes_count()

    def likes_count(self):
        """
        Получить количество лайков
        """
        return self.likes().count()

    def dislikes_count(self):
        """
        Получить количество дизлайков
        """
        return self.dislikes().count()

    def votes(self):
        """
        Получить статистику по рейтингу
        """
        return {
            "likes_count": self.likes_count(),
            "dislike_count": self.dislike_count(),
            "sum_rating": self.sum_rating()
        }

    def publications(self):
        return self.get_queryset().filter(content_type__model='publication').order_by('-publications__created')

    def comments(self):
        return self.get_queryset().filter(content_type__model='comment').order_by('-comments__created')


class Vote(TimestampedAbstractModel):
    """
    Модель оценок пользователя (Like/Dislike)
    """
    LIKE = 'LIKE'
    DISLIKE = 'DISLIKE'

    VOTES = (
        (DISLIKE, 'Не понравилось'),
        (LIKE, 'Понравилось')
    )

    vote = models.CharField(verbose_name="Голос", choices=VOTES, max_length=10)
    user = models.ForeignKey('User', verbose_name="Пользователь", on_delete=models.PROTECT)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    objects = VoteManager()

    class Meta:
        verbose_name = 'Оценка (Лайк/Дизлайк)'
        verbose_name_plural = 'Оценки (Лайки/Дизлайки)'
        ordering = ['-created']

    def __str__(self):
        return '[{}] Оценка "{}" от пользователя "{}" для объекта "{}"'.format(
            self.pk, self.get_vote_display(), self.user, self.content_object)
