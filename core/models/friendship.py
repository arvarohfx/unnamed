from django.db import models

from core.models import User
from core.models.common import TimestampedAbstractModel


class FriendshipRequest(TimestampedAbstractModel):
    from_user = models.ForeignKey('User', verbose_name='Пользователь, который отправлется запрос в друзья',
                                  related_name="invitations_from", on_delete=models.PROTECT)
    to_user = models.ForeignKey('User', verbose_name='Пользователь, которому отправляется запрос в друзья',
                                related_name='invitations_to', on_delete=models.PROTECT)
    message = models.CharField(verbose_name='Сообщение', max_length=200, blank=True)
    is_accepted = models.BooleanField(verbose_name='Принят', default=False)

    def accept(self):
        # Создание объектов "Дружбы" в случае их отсутствия
        Friendship.objects.get_or_create(user=self.from_user)
        Friendship.objects.get_or_create(user=self.to_user)
        # Добавление в друзья
        Friendship.objects.befriend(self.from_user, self.to_user)
        # Принятие запроса в друзья
        self.is_accepted = True
        self.save()

    def decline(self):
        self.delete()

    def cancel(self):
        self.delete()

    class Meta:
        verbose_name = 'Запрос в друзья'
        verbose_name_plural = 'Запросы в друзья'
        ordering = ['-created']

    def __str__(self):
        return '[{}] Запрос в друзья от "{}" к "{}"'.format(
            self.pk, self.from_user.get_full_name(), self.to_user.get_full_name())


class FriendshipManager(models.Manager):
    @staticmethod
    def friends_of(user, shuffle=False):
        """
        Получить список друзей пользователя
        """
        qs = User.objects.filter(friendship__friends__user=user)
        if shuffle:
            qs = qs.order_by('?')
        return qs

    @staticmethod
    def are_friends(user1, user2):
        """
        Проверка, являеются ли пользователи друзьями
        """
        try:
            friendship = Friendship.objects.get(user=user1)
        except Friendship.DoesNotExist:
            return False
        return bool(friendship.friends.filter(user=user2).exists())

    @staticmethod
    def befriend(user1, user2):
        """
        Сделать двух пользователей друзьями
        """
        friendship1, created = Friendship.objects.get_or_create(user=user1)
        friendship2, created = Friendship.objects.get_or_create(user=user2)
        friendship1.friends.add(friendship2)
        # Когда user1 принял запрос от user2, необходимо удалить запросы в друзья от user1 к user2
        FriendshipRequest.objects.filter(from_user=user1, to_user=user2).delete()

    @staticmethod
    def unfriend(user1, user2):
        """
        Разорвать дружеские связи между пользователями
        """
        # Разорвать связь между пользователями
        Friendship.objects.get(user=user1).friends.remove(Friendship.objects.get(user=user2))
        # Удалить запросы в друзья
        FriendshipRequest.objects.filter(from_user=user1, to_user=user2).delete()
        FriendshipRequest.objects.filter(from_user=user2, to_user=user1).delete()


class Friendship(TimestampedAbstractModel):
    """
    Модель дружбы между пользователями
    """
    user = models.OneToOneField('User', verbose_name='Текущий пользователья', related_name='friendship',
                                on_delete=models.PROTECT)
    friends = models.ManyToManyField('self', symmetrical=True)

    objects = FriendshipManager()

    class Meta:
        verbose_name = 'Дружба'
        verbose_name_plural = 'Дружба'

    def __str__(self):
        return '[{}] Друзья "{}"'.format(self.pk, self.user.get_full_name())
