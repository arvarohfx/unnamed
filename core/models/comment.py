from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from core.models.common import TimestampedAbstractModel
from core.models.vote import Vote


class Comment(TimestampedAbstractModel):
    """
    Модель комментариев к публикации
    """
    user = models.ForeignKey('User', verbose_name='Пользователь, оставивший комментарий', on_delete=models.PROTECT)
    publication = models.ForeignKey(
        'Publication', verbose_name='Публикация, к которой оставлен комментарий', on_delete=models.PROTECT)
    text = models.TextField(verbose_name='Текст комментария')
    is_edited = models.BooleanField(verbose_name='Комментарий был отредактирован', default=False)
    is_deleted = models.BooleanField(verbose_name='Удалено', default=False)

    votes = GenericRelation(Vote, related_query_name='comments')

    class Meta:
        verbose_name = 'Комментарий к публикации'
        verbose_name_plural = 'Комментарии к публикации'
        ordering = ['-created']

    def __str__(self):
        return '[{}] Комментарий от "{}" к публикации "{}"'.format(self.pk, self.user.username, self.publication)
