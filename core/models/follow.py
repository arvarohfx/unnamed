from django.db import models

from core.models.common import TimestampedAbstractModel


class Follow(TimestampedAbstractModel):
    """
    Модель подписок (followers)

    Для получения списка тех, на кого подписан пользователь: User.targets.all()
    Для получения списка подписчиков: User.followers.all()
    """
    target = models.ForeignKey('User', verbose_name='Цель подписки', related_name='followers', on_delete=models.PROTECT)
    follower = models.ForeignKey(
        'User', verbose_name='Подписавшийся пользователь', related_name='subscriptions', on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Подписка'
        verbose_name_plural = 'Подписки'

    def __str__(self):
        return '[{}] Подписка пользователя "{}" на пользователя "{}"'.format(self.pk, self.target, self.follower)
