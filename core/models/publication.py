import jsonfield as jsonfield
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from core.models.common import TimestampedAbstractModel
from core.models.vote import Vote

PUBLICATION_VISIBILITY_PUBLIC = 'PUBLIC'
PUBLICATION_VISIBILITY_FRIENDS = 'FRIENDS'
PUBLICATION_VISIBILITY_NOBODY = 'NOBODY'

PUBLICATION_VISIBILITY_CHOICES = (
    (PUBLICATION_VISIBILITY_PUBLIC, 'Видна всем'),
    (PUBLICATION_VISIBILITY_FRIENDS, 'Видна только друзьям'),
    (PUBLICATION_VISIBILITY_NOBODY, 'Видна только мне')
)


class Publication(TimestampedAbstractModel):
    """
    Модель публикации (поста)
    """
    user = models.ForeignKey('User', verbose_name='Пользователь, создавший публикацию', on_delete=models.PROTECT)
    data = jsonfield.JSONField(verbose_name='Данные о публикации')
    lat = models.DecimalField(verbose_name='Широта публикации', max_digits=9, decimal_places=6)
    lon = models.DecimalField(verbose_name='Долгота публикации', max_digits=9, decimal_places=6)
    visibility = models.CharField(verbose_name='Видимость публикации', choices=PUBLICATION_VISIBILITY_CHOICES,
                                  default=PUBLICATION_VISIBILITY_PUBLIC, max_length=128)
    is_deleted = models.BooleanField(verbose_name='Удалено', default=False)
    # TODO: Добавить флаг "Отключить комментарии" для того, чтобы комментарии отображались только владельцу публикации
    # TODO: и не было возможности оставлять новые комментарии

    votes = GenericRelation(Vote, related_query_name='publications')

    class Meta:
        verbose_name = 'Публикация'
        verbose_name_plural = 'Публикации'
        ordering = ['-created']

    def __str__(self):
        return '[{}] Публикация {}'.format(self.pk, self.user.username)
