import os
from uuid import uuid4

import phonenumbers
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.utils import timezone as django_timezone, timezone
from phonenumbers import NumberParseException

from core.models.common import TimestampedAbstractModel
from core.utils import is_valid_email

GENDER_MALE = 'MALE'
GENDER_FEMALE = 'FEMALE'

GENDER_CHOICES = (
    (GENDER_MALE, 'Мужской'),
    (GENDER_FEMALE, 'Женский')
)


def image_directory_path(instance, filename, subdir):
    # file will be uploaded to MEDIA_ROOT/img/<subdir>/<filename>
    ext = filename.split('.')[-1].lower()
    filename = '{}.{}'.format(uuid4(), ext)
    return os.path.join('images', subdir, filename)


def get_user_avatar_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/img/users/<filename>
    return image_directory_path(instance, filename, 'avatars')


class NormalizePhoneException(ValidationError):
    pass


class CustomUserManager(BaseUserManager):
    """
    Менеджер пользователей
    """
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        # username = self.model.normalize_username(username)
        # TODO: normalize_phone
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

    def get_user_by_username(self, username):
        """
        Найти пользователя по username (username или email).
        :param username: Никнейм или электронный адрес
        :return: Объект пользователя.
        """
        if is_valid_email(username):
            try:
                if is_valid_email(username):
                    return self.model.objects.get(email__iexact=username)
            except User.DoesNotExist:
                return None
        try:
            return self.model.objects.get(username=username)
        except User.DoesNotExist:
            return None


class User(AbstractUser, TimestampedAbstractModel):
    """
    Пользователь

    Обязательные поля: имя, email, телефон
    """
    # ===== переопределяем поле базового класса
    email = models.EmailField(verbose_name='Email', unique=True)
    username = models.CharField(verbose_name='Никнейм', max_length=255, unique=True)

    objects = CustomUserManager()
    USERNAME_FIELD = 'email'
    # обязательные поля при создании суперпользователя командой createsuperuser
    REQUIRED_FIELDS = ('first_name', 'last_name', 'username', )
    # =====

    phone = models.CharField(verbose_name='Телефон', max_length=255, help_text='Телефон в формате E.164', blank=True)
    first_name = models.CharField(verbose_name='Имя', max_length=255, blank=True)
    last_name = models.CharField(verbose_name='Фамилия', max_length=255, blank=True)
    middle_name = models.CharField(verbose_name='Отчество', max_length=255, blank=True)
    avatar = models.ImageField(verbose_name='Фото', blank=True, upload_to=get_user_avatar_directory_path,
                               max_length=1024)
    gender = models.CharField(verbose_name='Пол', choices=GENDER_CHOICES, max_length=20, blank=True, default='')
    birthday = models.DateField(verbose_name='День рождения', blank=True, null=True)
    registration_datetime = models.DateTimeField(verbose_name='Дата регистрации', default=django_timezone.now)

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        ordering = ['email']

    def __str__(self):
        return '[{}] {} {}'.format(self.pk, self.first_name, self.last_name)

    def get_full_name(self):
        return self.last_name + ' ' + self.first_name

    def get_short_name(self):
        return self.first_name

    @property
    def last_name_and_initials(self):
        """
        ФИО пользователя в формате 'Фамилия И.О.'
        :return:
        """
        result = ''
        if self.last_name:
            result += self.last_name + ' '
        if self.first_name:
            result += self.first_name[0] + '.'
        if self.middle_name:
            result += self.middle_name[0] + '.'
        return result

    @staticmethod
    def normalize_phone(phone):
        """
        Нормализация телефонного номера:
            - парсит строку с телефонным номером произвольного формата
            - проверяет номер на возможность (что он имеет правильное количество цифр)
            - проверяет номер на валидность (например, соблюдается NPA)
            - конвертирует номер в строку в формате E164
        :param phone: Телефонный номер в произвольном формате
        :return: Успех - нормализованный телефонный номер, неудача - Exception
        """
        # парсим телефонный номер и конвертируем его в объект <class 'phonenumbers.phonenumber.PhoneNumber'>
        try:
            phone_number = phonenumbers.parse(phone)
        except NumberParseException:
            # это вобще не телефонный номер
            raise NormalizePhoneException('Это не телефонный номер: {}'.format(phone))
        # проверяем, что номер возможен (имеет правильное количество цифр)
        if not phonenumbers.is_possible_number(phone_number):
            # невозможный номер
            raise NormalizePhoneException('Невозможный телефонный номер: {}'.format(phone))
        # проверяем, что номер валиден
        if not phonenumbers.is_valid_number(phone_number):
            raise NormalizePhoneException('Невалидный телефонный номер: {}'.format(phone))
        # конвертируем PhoneNumber в строку с телефонным номером стандарта E.164, пример: '+442083661177'
        phone_e164 = phonenumbers.format_number(phone_number, phonenumbers.PhoneNumberFormat.E164)
        return phone_e164

    def get_age(self):
        """
        Количество полных лет
        :return:
        """
        today = timezone.now().date()
        if not self.birthday:
            return ''
        return today.year - self.birthday.year - ((today.month, today.day) < (self.birthday.month, self.birthday.day))

    @property
    def friends(self):
        """
        Получить всех друзей пользователя
        """
        return User.objects.filter(
            Q(friendrelation__user=self) | Q(friendrelation__target_user=self),
            friendrelation__is_deleted=False).all()
