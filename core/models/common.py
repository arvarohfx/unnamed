from datetime import timedelta
from uuid import uuid4

from django.db import models
from django.db.models import Manager
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from core.utils import StringUtils


def generate_hash_uuid():
    return str(uuid4()).replace('-', '')


def two_minutes_gap():
    return timezone.now() + timedelta(minutes=2)


def five_minutes_gap():
    return timezone.now() + timedelta(minutes=5)


class VerificationCodeManager(Manager):
    def get_queryset(self):
        return super().get_queryset().filter(validated__isnull=True)


class TimestampedAbstractModel(models.Model):
    """
    Абстрактная модель, содержащая временные метки
    """
    created = models.DateTimeField(_('Время создания'), auto_now_add=True)
    modified = models.DateTimeField(_('Время модификации'), auto_now=True)

    class Meta:
        abstract = True
        ordering = ("-created",)


class VerificationCode(TimestampedAbstractModel):
    """Проверочные коды"""

    class CODES:
        VERIFICATION_SMS_CODE = 'verification_sms_code'
        VERIFICATION_EMAIL_CODE = 'verification_email_code'
        CHANGE_PHONE = 'change_phone_code'
        CHANGE_EMAIL = 'change_email_code'

        CHOICES = (
            (VERIFICATION_SMS_CODE, 'Код подтверждения номера телефона'),
            (VERIFICATION_EMAIL_CODE, 'Код подтверждения электронного адреса'),
            (CHANGE_PHONE, 'Изменения номера телефона'),
            (CHANGE_EMAIL, 'Изменения email адреса'),
        )

    verification_code = models.SlugField("Код восстановления", max_length=500)
    code_type = models.CharField("Тип кода восстановления", max_length=255, choices=CODES.CHOICES)
    email = models.EmailField("Email", max_length=255, blank=True, null=True)
    phone = models.CharField("Номер телефона", max_length=255, blank=True, null=True)
    hash_uuid = models.CharField("Уникальный код для ссылки", max_length=64, null=True, default=generate_hash_uuid)
    validated = models.DateTimeField("Время валидации", blank=True, null=True)
    expired = models.DateTimeField("Время истечения срока действия", blank=True, null=True, default=two_minutes_gap)
    ip_address = models.GenericIPAddressField("IP адрес", blank=True, null=True)

    objects = VerificationCodeManager()

    class Meta:
        ordering = ['-pk']
        verbose_name = 'Код подтверждения'
        verbose_name_plural = 'Коды подтверждения'

    @classmethod
    def create_sms_verification_code(cls, phone, ip_address):
        """
        Создание кода подтверждения телефона.
        :param phone:
        :param ip_address:
        :return:
        """
        return cls.objects.create(phone=phone, ip_address=ip_address,
                                  verification_code=StringUtils.generate_mobile_code(),
                                  code_type=cls.CODES.VERIFICATION_SMS_CODE)

    @classmethod
    def create_email_verification_code(cls, email, ip_address):
        """
        Создание кода подтверждения e-mail.
        :param email:
        :param ip_address:
        :return:
        """
        return cls.objects.create(email=email, ip_address=ip_address,
                                  verification_code=StringUtils.generate_email_code(),
                                  code_type=cls.CODES.VERIFICATION_EMAIL_CODE)

    @classmethod
    def create_short_email_verification_code(cls, email, ip_address):
        """
        Создание короткого вода подтверждения (используется в мобильных версиях)
        """
        return cls.objects.create(email=email, ip_address=ip_address,
                                  verification_code=StringUtils.generate_digit_code(length=6),
                                  code_type=cls.CODES.VERIFICATION_EMAIL_CODE)

    def _generate_code(self):
        if self.code_type == self.CODES.VERIFICATION_SMS_CODE:
            self.verification_code = StringUtils.generate_mobile_code()
        elif self.code_type == self.CODES.VERIFICATION_EMAIL_CODE:
            self.verification_code = StringUtils.generate_email_code()
        else:
            raise KeyError('Unknown code_type: {}'.format(self.code_type))

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        fields = []
        if not self.verification_code:
            fields.append('verification_code')
            self._generate_code()
        if fields:
            super().save(update_fields=fields)

    @classmethod
    def get_object_by_hash(cls, hash_uuid):
        if not hash_uuid:
            return
        try:
            return cls.objects.get(hash_uuid=hash_uuid, validated__isnull=True)
        except cls.DoesNotExist:
            return None

    @classmethod
    def get_object_by_code(cls, code):
        if not code:
            return
        try:
            return cls.objects.get(verification_code=code, validated__isnull=True, expired__gt=timezone.now())
        except cls.DoesNotExist:
            return None

    @classmethod
    def get_object_by_verification_code(cls, code):
        if not code:
            return
        return cls.objects.filter(verification_code=code, validated__isnull=True).order_by('-created').first()
