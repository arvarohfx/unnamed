from decouple import config
from django.conf import settings
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from oauth2_provider.admin import Application
from oauth2_provider.views import TokenView
from rest_framework.parsers import FormParser
from rest_framework.views import APIView


oauth2_app_token_grant_type = openapi.Parameter(
    name='grant_type',
    in_=openapi.IN_FORM,
    required=True,
    type=openapi.TYPE_STRING,
    description='Тип авторизации',
    default=Application.GRANT_PASSWORD
)

oauth2_app_token_username = openapi.Parameter(
    name='username',
    in_=openapi.IN_FORM,
    required=True,
    type=openapi.TYPE_STRING,
    description='Логин (email)',
    default=config('DJANGO_SU_EMAIL', default='arvarohfx@gmail.com')
)

oauth2_app_token_password = openapi.Parameter(
    name='password',
    in_=openapi.IN_FORM,
    required=True,
    type=openapi.TYPE_STRING,
    description='Пароль',
    default=config('DJANGO_SU_PASSWORD', default='admin')
)

oauth2_app_token_client_id = openapi.Parameter(
    name='client_id',
    in_=openapi.IN_FORM,
    required=True,
    type=openapi.TYPE_STRING,
    description='Идентификатор клиента',
    default=settings.OAUTH2_CLIENT_ID,
    pattern=r'\w{40}'
)

oauth2_app_token_client_secret = openapi.Parameter(
    name='client_secret',
    in_=openapi.IN_FORM,
    required=True,
    type=openapi.TYPE_STRING,
    description='Токен клиента',
    default=settings.OAUTH2_CLIENT_SECRET,
    pattern=r'\w{120}'
)


class TokenAPIView(TokenView, APIView):
    parser_classes = (FormParser, )

    @swagger_auto_schema(
        manual_parameters=[
            oauth2_app_token_grant_type,
            oauth2_app_token_username,
            oauth2_app_token_password,
            oauth2_app_token_client_id,
            oauth2_app_token_client_secret
        ],
        responses={
            '400': 'Неверные входные данные.',
            '401': 'Невалидный токен.',
            '500': 'Внутренняя ошибка сервера.',
            '200': 'Токен верифицирован.',
        },
    )
    def post(self, *args, **kwargs):
        """
        Получить oauth2 token

        Метод для получения oauth2 токена по email и паролю
        """
        return super().post(*args, **kwargs)
