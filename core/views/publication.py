from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from rest_framework.response import Response

from core.models import Publication, User
from core.models.publication import PUBLICATION_VISIBILITY_PUBLIC
from core.serializers.publication import PublicationSerializer, PublicationListRequestSerializer


class PublicationListView(generics.ListCreateAPIView):
    serializer_class = PublicationSerializer
    queryset = Publication.objects.filter(is_deleted=False)

    def get_serializer_context(self):
        return {"user": self.request.user}

    @swagger_auto_schema(
        query_serializer=PublicationListRequestSerializer,
        responses={
            '401': 'Необходима авторизация',
            '404': 'Пользователь, для которого запрашиваются публикации не найден',
            '500': 'Внутренняя ошибка сервера',
            '200': PublicationSerializer,
        },
    )
    def get(self, request, *args, **kwargs):
        """
        Получить список публикаций с фильтрацией

        Получить список всех доступных для просмотра публикаций с фильтрацией по координатам (прямоугольник),
        пользователю, времени публикации
        """
        request_serializer = PublicationListRequestSerializer(data=request.query_params)
        request_serializer.is_valid(raise_exception=True)

        lon_start = request_serializer.validated_data.get('lon_start')
        lon_end = request_serializer.validated_data.get('lon_end')
        lat_start = request_serializer.validated_data.get('lat_start')
        lat_end = request_serializer.validated_data.get('lat_end')
        user_id = request_serializer.validated_data.get('user_id')
        from_datetime = request_serializer.validated_data.get('from_datetime')
        to_datetime = request_serializer.validated_data.get('to_datetime')

        # TODO: Добавить все свои публикации и все видимые публикации друзей
        publications = Publication.objects.filter(is_deleted=False)

        # Фильтрация публикаций по пользователю
        if user_id:
            try:
                target_user = User.objects.get(pk=user_id)
            except User.DoesNotExist:
                return Response(data={'msg': 'Пользователь не существует'}, status=status.HTTP_404_NOT_FOUND)
            publications.filter(user=target_user, visibility__in=(PUBLICATION_VISIBILITY_PUBLIC, ))

        # Фильтрация публикаций по координатам
        if lon_start and lon_end and lat_start and lat_end:
            publications.filter(lon_gte=lon_start, lon_lte=lat_end, lat__gte=lat_start, lat_lte=lat_end)

        # Фильтрация по времени публикации
        if from_datetime:
            publications.filter(created__gte=from_datetime)
        if to_datetime:
            publications.filter(created__lte=to_datetime)

        publications = Publication.objects.filter(is_deleted=False)
        response_serializer = PublicationSerializer(publications, many=True)
        return Response(response_serializer.data)

    @swagger_auto_schema(
        request_body=PublicationSerializer,
        responses={
            '401': 'Необходима авторизация',
            '404': 'Пользователь, для которого запрашиваются публикации не найден',
            '500': 'Внутренняя ошибка сервера',
            '201': PublicationSerializer,
        },
    )
    def post(self, request, *args, **kwargs):
        """
        Создать новую публикацию

        Создание новой публикации с заданной видимостью
        """
        return self.create(request, *args, **kwargs)


class PublicationView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PublicationSerializer
    queryset = Publication.objects.all()

    @swagger_auto_schema(
        responses={
            '401': 'Необходима авторизация',
            '404': 'Публикация не найдена',
            '500': 'Внутренняя ошибка сервера',
            '200': PublicationSerializer,
        },
    )
    def get(self, request, *args, **kwargs):
        """
        Получить публикацию по id

        Получить подробную информацию по конкретной публикации
        """
        return self.retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        request_body=PublicationSerializer,
        responses={
            '400': 'Неверные входные данные',
            '401': 'Необходима авторизация',
            '404': 'Публикация не найдена',
            '500': 'Внутренняя ошибка сервера',
            '200': PublicationSerializer,
        },
    )
    def put(self, request, *args, **kwargs):
        """
        Полное обновление публикации

        Обновить все поля конкретной публикации
        """
        return self.update(request, *args, **kwargs)

    @swagger_auto_schema(
        request_body=PublicationSerializer,
        responses={
            '400': 'Неверные входные данные',
            '401': 'Необходима авторизация',
            '404': 'Публикация не найдена',
            '500': 'Внутренняя ошибка сервера',
            '200': PublicationSerializer,
        },
    )
    def patch(self, request, *args, **kwargs):
        """
        Частичное обновление публикации

        Частичное обновление конкретной публикации
        """
        return self.partial_update(request, *args, **kwargs)

    @swagger_auto_schema(
        request_body=PublicationSerializer,
        responses={
            '401': 'Необходима авторизация',
            '404': 'Публикация не найдена',
            '500': 'Внутренняя ошибка сервера',
            '204': 'Публикация удалена',
        },
    )
    def delete(self, request, *args, **kwargs):
        """
        Удаление публикации

        Удалить публикацию (публикация не удаляется окончательно)
        """
        return self.destroy(request, *args, **kwargs)

    def perform_destroy(self, instance):
        """
        Перезапись метода для удаления экземпляра
        """
        instance.is_deleted = True
        instance.save()
