from datetime import timedelta

from django.utils import timezone
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import Publication
from core.models.comment import Comment
from core.serializers.comment import CommentSerializer, CommentCreateRequestSerializer, CommentListRequestSerializer, \
    CommentUpdateRequestSerializer


class CommentListView(APIView):
    """
    Создать комментарий для публикации или получить список комментариев для конкретной публикации
    """
    @swagger_auto_schema(
        query_serializer=CommentListRequestSerializer,
        responses={
            '400': 'Неверные входные данные',
            '401': 'Необходима аутентификация',
            '403': 'Пользователи не имеет прав просматривать комментарии',
            '404': 'Публикация не найдена',
            '500': 'Внутренняя ошибка сервера',
            '200': CommentSerializer,
        }
    )
    def get(self, request):
        """
        Получить список комментариев к публикации

        Получить полный список комментариев для публикации. Доступен пэйджинг
        """
        # user = request.user
        request_serializer = CommentListRequestSerializer(data=request.query_params)
        request_serializer.is_valid(raise_exception=True)
        limit = request_serializer.validated_data.get('limit', None)
        offset = request_serializer.validated_data.get('offset', 0)
        publication_id = request_serializer.validated_data['publication_id']

        if limit is not None:
            limit = offset + limit

        # Получить публикацию
        try:
            publication = Publication.objects.get(pk=publication_id)
        except Publication.DoesNotExist:
            return Response(data={'msg': 'Публикация не найдена'}, status=status.HTTP_404_NOT_FOUND)
        # TODO: Проверка: пользователь имеет право просматривать комментарии к публикации

        comments = publication.comment_set.filter(is_deleted=False).order_by('-created')[offset:limit]
        response_serializer = CommentSerializer(comments, many=True)
        return Response(response_serializer.data)

    @swagger_auto_schema(
        request_body=CommentCreateRequestSerializer,
        responses={
            '400': 'Неверные входные данные',
            '401': 'Необходима аутентификация',
            '403': 'Пользователь не имеет права оставлять комментарий к записи',
            '404': 'Публикация не найдена',
            '429': 'Вы оставляете комментарии слишком часто',
            '500': 'Внутренняя ошибка сервера',
            '201': CommentSerializer,
        }
    )
    def post(self, request):
        """
        Добавить новый комментарий к публикации

        Добавить комментарий к публикации
        """
        user = request.user
        request_serializer = CommentCreateRequestSerializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        text = request_serializer.validated_data['text']
        publication_id = request_serializer.validated_data['publication_id']

        try:
            publication = Publication.objects.get(pk=publication_id)
        except Publication.DoesNotExist:
            return Response(data={'msg': 'Публикация не найдена'}, status=status.HTTP_404_NOT_FOUND)

        # TODO: Проверка: пользователь имеет право оставлять комментарий под публикацией

        # Проверка: нельзя оставлять комментарии слишком часто (максимум 10 комментариев за 5 минут)
        if Comment.objects.filter(
                user=user, created__range=(timezone.now() - timedelta(minutes=5), timezone.now())).count() > 10:
            return Response(
                data={'msg': 'Вы оставляете комментарии слишком часто'}, status=status.HTTP_429_TOO_MANY_REQUESTS)

        comment = Comment.objects.create(
            user=user,
            publication=publication,
            text=text
        )
        response_serializer = CommentSerializer(comment)
        return Response(response_serializer.data, status=status.HTTP_201_CREATED)


class CommentView(APIView):
    """
    Отредактировать/удалить комментарий
    """
    @swagger_auto_schema(
        request_body=CommentUpdateRequestSerializer,
        responses={
            '400': 'Неверные входные данные',
            '401': 'Необходима аутентификация',
            '403': 'Пользователь не имеет права изменять этот комментарий',
            '404': 'Комментарий не найден',
            '500': 'Внутренняя ошибка сервера',
            '200': CommentSerializer,
        }
    )
    def patch(self, request, *args, **kwargs):
        """
        Изменить комментарий к публикации

        Изменить комментарий к публикации. Изменить комментарий можно только в течении полу часа после публикации.
        После изменения комментарий помечается флагом is_edited.
        """
        user = request.user
        request_serializer = CommentUpdateRequestSerializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        comment_id = kwargs['pk']
        text = request_serializer.validated_data['text']

        # Найдём комментарий для редактирования
        try:
            comment = Comment.objects.get(pk=comment_id)
        except Comment.DoesNotExist:
            return Response(data={'msg': 'Комментарий не найден'}, status=status.HTTP_404_NOT_FOUND)
        # TODO: Проверка: права других пользователей редактировать комментарий (мб владелец поста)
        # Проверка: пользователь является владельцем комментария
        if comment.user != user:
            return Response(data={'msg': 'Комментарий не принадлежит пользователю'}, status=status.HTTP_403_FORBIDDEN)

        # Проверка: с момента публикации комментария прошло больше 30 минут
        if comment.created + timedelta(minutes=30) < timezone.now():
            return Response(data={'msg': 'Нельзя изменять комментарий, прошло больше 30 минут с момента публикации'},
                            status=status.HTTP_403_FORBIDDEN)

        # Изменить комментарий
        comment.text = text
        comment.is_edited = True
        comment.save()

        response_serializer = CommentSerializer(comment)
        return Response(response_serializer.data)

    @swagger_auto_schema(
        responses={
            '400': 'Неверные входные данные',
            '401': 'Необходима аутентификация',
            '403': 'Пользователь не имеет права удалять этот комментарий',
            '404': 'Комментарий не найден',
            '500': 'Внутренняя ошибка сервера',
            '204': 'Удалено',
        }
    )
    def delete(self, request, *args, **kwargs):
        """
        Удалить комментарий к публикации

        Удалить комментарий к публикации. Удалённый комментарий остаётся в базе, но помечается флагом is_deleted
        и не возвращается в методах
        """
        user = request.user
        comment_id = kwargs['pk']

        # Найдём комментарий для редактирования
        try:
            comment = Comment.objects.get(pk=comment_id)
        except Comment.DoesNotExist:
            return Response(data={'msg': 'Комментарий не найден'}, status=status.HTTP_404_NOT_FOUND)
        # TODO: Проверка: права других пользователей удалять комментарий (мб владелец поста)
        # Проверка: пользователь является владельцем комментария
        if comment.user != user:
            return Response(data={'msg': 'Комментарий не принадлежит пользователю'}, status=status.HTTP_403_FORBIDDEN)

        # Удалить комментарий
        comment.is_deleted = True
        comment.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
