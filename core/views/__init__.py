from core.views.comment import CommentView, CommentListView
from core.views.follower import FollowerListView, FollowerView
from core.views.friendship import FriendshipListView, FriendshipView
from core.views.friendship_request import FriendRequestListView, FriendRequestView
from core.views.oauth2_token import TokenAPIView
from core.views.publication import PublicationView, PublicationListView
from core.views.user import UserView
from core.views.user_email_confirmation import UserEmailConfirmation
from core.views.user_register import UserRegister
from core.views.user_reset_password import UserPasswordReset
from core.views.vote import VoteView


__all__ = [TokenAPIView, UserRegister, UserView, UserEmailConfirmation, UserPasswordReset, PublicationView,
           PublicationListView, FriendRequestListView, FriendRequestView, FriendshipListView, FriendshipView,
           CommentView, CommentListView, VoteView, FollowerListView, FollowerView]
