from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.parsers import FormParser, MultiPartParser, JSONParser
from rest_framework.permissions import IsAuthenticated

from core.models import User
from core.serializers.user import UserSerializer


class UserView(RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated, )
    parser_classes = (FormParser, MultiPartParser, JSONParser)

    @swagger_auto_schema(
        responses={
            '400': 'Неверные входные данные',
            '401': 'Необходима авторизация',
            '404': 'Пользователь не найден',
            '500': 'Внутренняя ошибка сервера',
            '200': UserSerializer,
        },
    )
    def get(self, request, *args, **kwargs):
        """
        Получить пользователя по id

        Получение пользователя по id
        Требуется аутентификация
        """
        return self.retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={
            '400': 'Неверные входные данные',
            '401': 'Необходима авторизация',
            '404': 'Пользователь не найден',
            '500': 'Внутренняя ошибка сервера',
            '200': UserSerializer,
        },
    )
    def put(self, request, *args, **kwargs):
        """
        Полное обновление пользователя

        Обновить все поля пользователя
        Требуется аутентификация
        """
        return self.update(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={
            '400': 'Неверные входные данные',
            '401': 'Необходима авторизация',
            '404': 'Пользователь не найден',
            '500': 'Внутренняя ошибка сервера',
            '200': UserSerializer,
        },
    )
    def patch(self, request, *args, **kwargs):
        """
        Частичное обновление пользователя

        Обновить выбранные поля пользователя
        Требуется аутентификация
        """
        return self.partial_update(request, *args, **kwargs)
