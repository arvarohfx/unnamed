from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from rest_framework.response import Response

from core.models import User
from core.models.friendship import Friendship, FriendshipRequest
from core.serializers.friendship_request import FriendshipRequestSerializer, FriendshipRequestListSerializer, \
    FriendRequestCreateSerializer


class FriendRequestListView(generics.ListCreateAPIView):
    serializer_class = FriendshipRequestSerializer
    queryset = FriendshipRequest.objects.all()

    @swagger_auto_schema(
        responses={
            '401': 'Необходима авторизация',
            '500': 'Внутренняя ошибка сервера',
            '200': FriendshipRequestListSerializer,
        },
    )
    def get(self, request, *args, **kwargs):
        """
        Получить список входящих и исходящих запросов в друзья

        Получить список новых входящих и исходяших запросов в друзья (отменённые и отклонённые игнорируются)
        """
        user = request.user
        response_serializer = FriendshipRequestListSerializer({}, context={'user': user})
        return Response(response_serializer.data)

    @swagger_auto_schema(
        request_body=FriendRequestCreateSerializer,
        responses={
            '400': 'Неверные входные параметры',
            '401': 'Необходима авторизация',
            '403': 'Нельзя добавить пользователя в друзья',
            '404': 'Пользователь для добавления в друзья не найден',
            '409': 'Пользователь уже находится у вас в друзьях',
            '500': 'Внутренняя ошибка сервера',
            '201': FriendshipRequestSerializer,
        },
    )
    def post(self, request, *args, **kwargs):
        """
        Создать новый запрос на добавление в друзья

        Создание нового запроса на добавление в друзья. Запрос создаётся в статусе "NEW"
        """
        user = request.user
        serializer = FriendRequestCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        target_user_id = serializer.validated_data['target_user_id']

        try:
            target_user = User.objects.get(pk=target_user_id)
        except User.DoesNotExist:
            return Response(data={'msg': 'Пользователь для добавления в друзья не найден'},
                            status=status.HTTP_404_NOT_FOUND)

        if target_user == user:
            return Response(data={'msg': 'Нельзя добавить в друзья самого себя'}, status=status.HTTP_403_FORBIDDEN)

        # Проверка: пользователь уже в друзьях
        if Friendship.objects.are_friends(user, target_user):
            return Response(data={'msg': 'Пользователь уже находится у вас в друзьях'}, status=status.HTTP_409_CONFLICT)
        # TODO: Проверка - пользователь занёс вас в чёрный список

        friend_request = FriendshipRequest.objects.create(from_user=user, to_user=target_user)

        response_serializer = FriendshipRequestSerializer(friend_request)
        return Response(response_serializer.data, status=status.HTTP_201_CREATED)


class FriendRequestView(generics.RetrieveDestroyAPIView):
    serializer_class = FriendshipRequestSerializer
    queryset = FriendshipRequest.objects.all()

    @swagger_auto_schema(
        responses={
            '401': 'Необходима авторизация',
            '404': 'Запрос в друзья не найден',
            '500': 'Внутренняя ошибка сервера',
            '200': FriendshipRequestSerializer,
        },
    )
    def get(self, request, *args, **kwargs):
        """
        Получить запрос в друзья по id

        Получить подробную информацию по конкретному запросу в друзья
        """
        return self.retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={
            '401': 'Необходима авторизация',
            '403': 'Пользователь не является целью данного запроса в друзья',
            '404': 'Запрос в друзья не найден',
            '500': 'Внутренняя ошибка сервера',
            '200': FriendshipRequestSerializer,
        },
    )
    def patch(self, request, *args, **kwargs):
        """
        Принять запрос в друзья

        Запрос переходит в статус "Принято"
        """
        user = request.user
        try:
            friendship_request = FriendshipRequest.objects.get(pk=kwargs.get('pk'))
        except FriendshipRequest.DoesNotExist:
            return Response(data={"msg": "Запрос в друзья не найден"}, status=status.HTTP_404_NOT_FOUND)
        if friendship_request.to_user != user:
            return Response(data={"msg": "Пользователь не является целью данного запроса в друзья"},
                            status=status.HTTP_403_FORBIDDEN)
        friendship_request.accept()

        serializer = FriendshipRequestSerializer(friendship_request)
        return Response(serializer.data)

    @swagger_auto_schema(
        responses={
            '401': 'Необходима авторизация',
            '404': 'Публикация не найдена',
            '500': 'Внутренняя ошибка сервера',
            '204': 'Запрос в друзья отменён/отклонён',
        },
    )
    def delete(self, request, *args, **kwargs):
        """
        Отмена/отклонение запроса в друзья

        Отменить или отклонить запрос в друзья
        Если метод вызывает пользователь, который создал этот запрос - запрос переходит в статус "Отменено"
        Если метод вызывает пользователь, который является целью запроса - запрос переходит в статус "Отклонено"
        """
        return self.destroy(request, *args, **kwargs)

    def perform_destroy(self, instance):
        """
        Перезапись метода для удаления экземпляра
        """
        current_user = self.request.user
        if current_user == instance.from_user:
            instance.cancel()
        if current_user == instance.to_user:
            instance.decline()
