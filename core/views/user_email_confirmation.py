from django.utils import timezone
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import User
from core.models.common import VerificationCode
from core.serializers.user_email_confirmation import UserEmailConfirmationRequestSerializer


class UserEmailConfirmation(APIView):
    """
    Подтверждение электронного адреса

    Подтверждение электронного адреса по коду верификации, высланному на email при регистрации пользователя
    """
    permission_classes = (permissions.AllowAny, )

    @swagger_auto_schema(
        request_body=UserEmailConfirmationRequestSerializer,
        responses={
            '400': 'Неверные входные данные',
            '404': 'Пользователь не найден или код верификации устарел или не существует',
            '200': 'Электронный адрес подтверждён',
            '500': 'Внутренняя ошибка сервера'
        },
    )
    def post(self, request):
        request_serializer = UserEmailConfirmationRequestSerializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        verification_code = request_serializer.validated_data.get('verification_code')

        # Поиск актуального неподтверждённого кода верификации по uuid
        verification_code = VerificationCode.get_object_by_verification_code(verification_code)
        if not verification_code:
            return Response(data={'msg': 'Код верификации устарел или не существует'}, status=status.HTTP_404_NOT_FOUND)

        # Поиск пользователя, который подтверждает код
        try:
            user = User.objects.get(email=verification_code.email)
        except User.DoesNotExist:
            return Response(data={'msg': 'Пользователь не найден'}, status=status.HTTP_404_NOT_FOUND)

        # Если пользователь ещё не активирован -  Активация пользователя и подтверждение кода верификации
        if not user.is_active:
            user.is_active = True
            user.save()
            verification_code.validated = timezone.now()
            verification_code.save()

        return Response(data={'msg': 'Электронный адрес подтверждён', 'hash_uuid': verification_code.hash_uuid})
