from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from rest_framework.response import Response

from core.models import User
from core.models.friendship import Friendship
from core.serializers.user import PublicUserSerializer


class FriendshipListView(generics.ListAPIView):
    serializer_class = PublicUserSerializer
    queryset = Friendship.objects.all()

    def get_queryset(self):
        user = self.request.user
        friends = Friendship.objects.friends_of(user)
        return friends

    @swagger_auto_schema(
        responses={
            '400': 'Неверные входные данные',
            '401': 'Необходима аутентификация',
            '500': 'Внутренняя ошибка сервера',
            '200': PublicUserSerializer,
        }
    )
    def get(self, request, *args, **kwargs):
        """
        Список друзей

        Получить полный список друзей пользователя
        """
        return self.list(request, *args, **kwargs)


class FriendshipView(generics.DestroyAPIView):

    @swagger_auto_schema(
        responses={
            '400': 'Неверные входные данные',
            '401': 'Необходима аутентификация',
            '403': 'Пользователи не являются друзьями',
            '404': 'Пользователь не найден',
            '500': 'Внутренняя ошибка сервера',
            '200': 'Пользователь убран из друзей',
        }
    )
    def delete(self, request, *args, **kwargs):
        """
        Удалить из друзей

        Удалить пользователя из друзей
        """
        current_user = request.user
        user_id = kwargs.get('user_id')
        try:
            target_user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            return Response(data={'msg': 'Пользователь не найден'}, status=status.HTTP_404_NOT_FOUND)

        if not Friendship.objects.are_friends(current_user, target_user):
            return Response(data={'msg': 'Пользователи не являются друзьями'}, status=status.HTTP_403_FORBIDDEN)

        if current_user == target_user:
            return Response(data={'msg': 'Невозможно удалить из друзей самого себя'}, status=status.HTTP_403_FORBIDDEN)

        # Убрать из друзей
        Friendship.objects.unfriend(current_user, target_user)

        return Response(data={'msg': 'Пользователь удалён из друзей'}, status=status.HTTP_204_NO_CONTENT)
