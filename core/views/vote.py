from django.contrib.contenttypes.models import ContentType
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models.vote import Vote
from core.serializers.vote import VoteSerializer


class VoteView(APIView):
    """
    Оценка (Лайк/Дизлайк) объекта
    """
    model = None

    @swagger_auto_schema(
        request_body=VoteSerializer,
        responses={
            '401': 'Необходима авторизация',
            '403': 'Вы уже ставили эту оценку этому объекту',
            '404': 'Оцениваемый объект не найден',
            '500': 'Внутренняя ошибка сервера',
            '200': 'Оценено',
        },
    )
    def post(self, request, pk):
        """
        Поставить лайк/дизлайк

        Устанавливает лайк или дизлайк для выбранного объекта. Повторное проставление такой же оценки вернёт статус 200.
        При отправке дизлайка (при существующей оценке "Лайк") - оценка изменяется на дизлайк.
        """
        user = request.user
        # Найти оцениваемый объект
        try:
            instance = self.model.objects.get(pk=pk)
        except self.model.DoesNotExist:
            return Response(data={'msg': 'Оцениваемый объект не найден'}, status=status.HTTP_404_NOT_FOUND)
        serializer = VoteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        vote = serializer.validated_data['vote']

        # Попробовать найти оценку для этого объекта
        obj_vote, created = Vote.objects.get_or_create(
            user=user,
            content_type=ContentType.objects.get_for_model(instance),
            object_id=instance.pk,
            defaults={"vote": vote}
        )
        # Если оценка уже была - обновить её
        if not created:
            obj_vote.vote = vote
            obj_vote.save()
        return Response(data={'msg': 'Оценка добавлена'})
