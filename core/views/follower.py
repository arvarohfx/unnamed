from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import User
from core.models.follow import Follow
from core.serializers.follow import FollowListSerializer, FollowCreateSerializer


class FollowerListView(APIView):

    @swagger_auto_schema(
        responses={
            '401': 'Необходима авторизация',
            '500': 'Внутренняя ошибка сервера',
            '200': FollowListSerializer,
        }
    )
    def get(self, request, *args, **kwargs):
        """
        Получить список подписок и подписчиков

        Получить список пользователей, на которых вы подписаны и пользователей, подписанных на вас
        """
        user = request.user
        response_serializer = FollowListSerializer({}, context={'user': user})
        return Response(response_serializer.data)

    @swagger_auto_schema(
        request_body=FollowCreateSerializer,
        responses={
            '400': 'Неверные входные параметры',
            '401': 'Необходима авторизация',
            '403': 'Нельзя подписаться на пользователя',
            '404': 'Пользователь для подписки не найден',
            '409': 'Подписка на указанного пользователя уже есть',
            '500': 'Внутренняя ошибка сервера',
            '201': FollowListSerializer,
        }
    )
    def post(self, request, *args, **kwargs):
        """
        Подписаться на пользователя

        Подписаться на пользователя с целью отслеживания контента, который он добавляет
        """
        user = request.user
        serializer = FollowCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        target_user_id = serializer.validated_data['target_user_id']

        try:
            target_user = User.objects.get(pk=target_user_id)
        except User.DoesNotExist:
            return Response(data={'msg': 'Пользователь для подписки не найден'},
                            status=status.HTTP_404_NOT_FOUND)

        # Проверка: подписка на самого себя
        if target_user == user:
            return Response(data={'msg': 'Нельзя подписаться на самого себя'}, status=status.HTTP_403_FORBIDDEN)

        # Проверка: подписка уже существует
        if Follow.objects.filter(follower=user, target=target_user).exists():
            return Response(data={'msg': 'Вы уже подписаны на пользователя'}, status=status.HTTP_409_CONFLICT)

        # Создать подписку
        Follow.objects.create(follower=user, target=target_user)

        response_serializer = FollowListSerializer({}, context={'user': user})
        return Response(response_serializer.data, status=status.HTTP_201_CREATED)


class FollowerView(APIView):

    @swagger_auto_schema(
        responses={
            '401': 'Необходима авторизация',
            '404': 'Пользователь или подписка не найдены',
            '500': 'Внутренняя ошибка сервера',
            '204': 'Вы отписались от пользователя',
        }
    )
    def delete(self, request, user_id):
        """
        Отписка от пользователя

        Отписаться от указанного пользователя
        """
        user = request.user

        try:
            target_user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return Response(data={'msg': 'Пользователь не найден'}, status=status.HTTP_404_NOT_FOUND)

        # Получить объект подписки
        follow = Follow.objects.filter(target=target_user, follower=user).first()

        if not follow:
            return Response(data={'msg': 'Подписка не найдена'}, status=status.HTTP_404_NOT_FOUND)

        # Удалить подписку
        follow.delete()

        return Response(data={'msg': 'Вы отписались от пользователя'}, status=status.HTTP_204_NO_CONTENT)
