from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions
from rest_framework.generics import CreateAPIView

from core.serializers.user import UserSerializer


class UserRegister(CreateAPIView):
    """
    Создание нового пользователя

    Создание нового пользователя (регистрация)
    """
    serializer_class = UserSerializer
    permission_classes = (permissions.AllowAny, )

    def get_serializer_context(self):
        return {'request': self.request}

    @swagger_auto_schema(
        responses={
            '400': 'Неверные входные данные',
            '201': UserSerializer,
            '500': 'Внутренняя ошибка сервера'
        },
    )
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
