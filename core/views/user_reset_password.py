from django.utils import timezone
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import User
from core.models.common import VerificationCode
from core.serializers.user_reset_password import UserResetPasswordRequestSerializer
from core.utils import get_client_ip


class UserPasswordReset(APIView):

    permission_classes = (permissions.AllowAny, )

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            required=['email', ],
            properties={'email': openapi.Schema(type=openapi.TYPE_STRING, description='Электронный адрес')}
        ),
        responses={
            '400': 'Неверные входные данные',
            '404': 'Пользователь не существует',
            '200': 'На указанный электронный адрес отправлен код для восстановления',
            '500': 'Внутренняя ошибка сервера'
        },
    )
    def post(self, request):
        """
        Восстановление пароля: отправка кода верификации

        Отправка кода верификации электронного адреса на указанный email
        """
        email = request.data.get('email', None)
        # Проверка: необходимые поля
        if email is None:
            return Response(data={'email': ['Это поле обязательно']}, status=status.HTTP_400_BAD_REQUEST)

        # Получить пользователя
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            return Response(data={'msg': 'Пользователь не найден'}, status=status.HTTP_404_NOT_FOUND)

        VerificationCode.create_short_email_verification_code(user.email, get_client_ip(request))
        # TODO: Отправка кода верификации на электронный адрес

        return Response(data={'msg': 'Код верификации отправлен на указанный электронный адрес'})

    @swagger_auto_schema(
        request_body=UserResetPasswordRequestSerializer,
        responses={
            '400': 'Неверные входные данные',
            '404': 'Код верификации или пользователь не найден',
            '200': 'Пароль изменён',
            '500': 'Внутренняя ошибка сервера'
        },
    )
    def patch(self, request):
        """
        Восстановление пароля: задание нового пароля

        Задание нового пароля по uuid, который вернул метод верификации электронного адреса
        """
        request_serializer = UserResetPasswordRequestSerializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        hash_uuid = request_serializer.validated_data.get('hash_uuid')
        new_password = request_serializer.validated_data.get('new_password')

        verification_code = VerificationCode.get_object_by_hash(hash_uuid)
        if not verification_code:
            return Response(data={'msg': 'Код верификации не найден'}, status=status.HTTP_404_NOT_FOUND)
        try:
            user = User.objects.get(email=verification_code.email)
        except User.DoesNotExist:
            return Response(data={'msg': 'Пользователь не найден'}, status=status.HTTP_404_NOT_FOUND)

        # Валидация кода подтверждения
        verification_code.validated = timezone.now()
        verification_code.save()

        # Задание нового пароля
        user.set_password(new_password)
        user.save()
        return Response(data={'msg': 'Пароль изменён'})
