from rest_framework import serializers

from core.serializers.user import PublicUserSerializer


class FollowCreateSerializer(serializers.Serializer):
    """
    Сериализатор для подписки на пользователя
    """
    target_user_id = serializers.IntegerField(
        help_text='Идентификатор пользователя, на которого подписывается пользователь')

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class FollowListSerializer(serializers.Serializer):
    subscriptions = serializers.SerializerMethodField(help_text='Все подписки текущего пользователя')
    followers = serializers.SerializerMethodField(help_text='Подписчики текущего пользователя')

    def get_subscriptions(self, obj):
        """
        Получить все подписки
        """
        user = self.context.get('user')
        if not user:
            raise NotImplementedError('Для получения списка подписок в сериализатор необходимо передать пользователя')
        subscriptions = user.subscriptions.all()
        serializer = PublicUserSerializer(subscriptions, many=True)
        return serializer.data

    def get_followers(self, obj):
        """
        Получить всех подписчиков
        """
        user = self.context.get('user')
        if not user:
            raise NotImplementedError('Для получения подписчиков в сериализатор необходимо передать пользователя')
        followers = user.followers.all()
        serializer = PublicUserSerializer(followers, many=True)
        return serializer.data

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    class Meta:
        fields = ('subscriptions', 'followers', )
        read_only_fields = fields
