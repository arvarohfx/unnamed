from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from rest_framework import serializers


class UserResetPasswordRequestSerializer(serializers.Serializer):
    """
    Сериализатор запроса на подтверждение электронного адреса
    """
    hash_uuid = serializers.CharField(
        help_text='Уникальная строка на основе UUID4, возвращаемая в методе подтверждения электронного адреса')
    new_password = serializers.CharField(help_text='Пароль')

    @staticmethod
    def validate_new_password(new_password):
        try:
            validate_password(new_password)
        except ValidationError as e:  # не путать, это ValidationError из django
            # e.messages содержит список ошибок валидации, возвращаем первую
            raise serializers.ValidationError(e.messages[0])
        return new_password

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    class Meta:
        fields = ('hash_uuid', 'new_password', )
