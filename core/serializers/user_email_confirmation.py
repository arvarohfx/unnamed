from rest_framework import serializers


class UserEmailConfirmationRequestSerializer(serializers.Serializer):
    """
    Сериализатор запроса на подтверждение электронного адреса
    """
    verification_code = serializers.CharField(help_text='Код подтверждения электронного адреса')

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    class Meta:
        fields = ('verification_code', )
