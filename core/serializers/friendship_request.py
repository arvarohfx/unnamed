from rest_framework import serializers

from core.models.friendship import FriendshipRequest
from core.serializers.user import PublicUserSerializer


class FriendRequestCreateSerializer(serializers.Serializer):
    """
    Сериализатор для создания запроса в друзья
    """
    target_user_id = serializers.IntegerField(help_text='Идентификатор пользователя, которого добавляют в друзья')

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class FriendshipRequestListSerializer(serializers.Serializer):
    incoming = serializers.SerializerMethodField(help_text='Входящие заявки в друзья')
    outgoing = serializers.SerializerMethodField(help_text='Исходящие заявки в друзья')

    def get_incoming(self, obj):
        """
        Получить все входящие заявки в друзья в статусе "Новая"
        """
        user = self.context.get('user')
        if not user:
            raise NotImplementedError('Для получения списка заявок в сериализатор необходимо передать пользователя')
        incoming_friend_requests = FriendshipRequest.objects.filter(to_user=user, is_accepted=False)
        serializer = FriendshipRequestSerializer(incoming_friend_requests, many=True)
        return serializer.data

    def get_outgoing(self, obj):
        """
        Получить все исходящие заявки в друзья в статусе "Новая"
        """
        user = self.context.get('user')
        if not user:
            raise NotImplementedError('Для получения списка заявок в сериализатор необходимо передать пользователя')
        outgoing_friend_requests = FriendshipRequest.objects.filter(from_user=user, is_accepted=False)
        serializer = FriendshipRequestSerializer(outgoing_friend_requests, many=True)
        return serializer.data

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    class Meta:
        fields = ('incoming', 'outgoing', )
        read_only_fields = fields


class FriendshipRequestSerializer(serializers.ModelSerializer):
    """
    Сериализатор модели запроса в друзья
    """
    from_user = PublicUserSerializer(help_text='Пользователь, который отправил запрос в друзья')
    to_user = PublicUserSerializer(help_text='Пользователь, которому отправили запрос в друзья')

    @staticmethod
    def get_status_display(obj):
        return obj.get_status_display()

    class Meta:
        model = FriendshipRequest
        fields = ('id', 'created', 'from_user', 'to_user', 'message', 'is_accepted', )
        read_only_fields = fields
