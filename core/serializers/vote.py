from rest_framework import serializers

from core.models.vote import Vote


class VoteSerializer(serializers.ModelSerializer):
    vote = serializers.ChoiceField(choices=Vote.VOTES)

    class Meta:
        model = Vote
        fields = ('id', 'created', 'modified', 'vote', 'object_id', 'content_type', )
        read_only_fields = ('id', 'created', 'modified', 'object_id', 'content_type', )
