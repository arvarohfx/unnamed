from rest_framework import serializers

from core.models.comment import Comment
from core.serializers.common import PaginationRequestSerializer
from core.serializers.user import PublicUserSerializer


class CommentListRequestSerializer(PaginationRequestSerializer):
    """
    Сериализатор для получения списка комментариев к публикации
    """
    publication_id = serializers.IntegerField(required=True, help_text='Идентификатор публикации')

    class Meta:
        fields = ('offset', 'limit', 'publication_id', )
        read_only_fields = fields


class CommentCreateRequestSerializer(serializers.ModelSerializer):
    """
    Сериализатор модели комментария к публикации для запроса на создание
    """
    publication_id = serializers.IntegerField(required=True, help_text='Идентификатор публикации')

    class Meta:
        model = Comment
        fields = ('publication_id', 'text', )


class CommentUpdateRequestSerializer(serializers.ModelSerializer):
    """
    Сериализатор для обновления комментария
    """

    class Meta:
        model = Comment
        fields = ('text', )


class CommentSerializer(serializers.ModelSerializer):
    """
    Сериализатор модели комментария к публикации
    """
    user = PublicUserSerializer(help_text='Пользователь, оставивший комментарий')
    likes_count = serializers.SerializerMethodField()
    dislikes_count = serializers.SerializerMethodField()

    @staticmethod
    def get_likes_count(obj):
        """
        Получить количество лайков публикации
        """
        return obj.votes.likes_count()

    @staticmethod
    def get_dislikes_count(obj):
        """
        Получить количество дизлайков публикации
        """
        return obj.votes.dislikes_count()

    class Meta:
        model = Comment
        fields = ('id', 'created', 'modified', 'user', 'text', 'is_edited', 'likes_count', 'dislikes_count', )
        read_only_fields = fields
