from rest_framework import serializers

from core.models import Publication
from core.serializers.comment import CommentSerializer
from core.serializers.user import PublicUserSerializer


class PublicationListRequestSerializer(serializers.Serializer):
    """
    Сериаилазтор запроса списка публикаций
    """
    lon_start = serializers.DecimalField(
        help_text='Фильтрация по координатам', max_digits=9, decimal_places=6, required=False)
    lon_end = serializers.DecimalField(
        help_text='Фильтрация по координатам', max_digits=9, decimal_places=6, required=False)
    lat_start = serializers.DecimalField(
        help_text='Фильтрация по координатам', max_digits=9, decimal_places=6, required=False)
    lat_end = serializers.DecimalField(
        help_text='Фильтрация по координатам', max_digits=9, decimal_places=6, required=False)
    user_id = serializers.IntegerField(help_text='Фильтрация по пользователю', required=False)
    from_datetime = serializers.DateTimeField(help_text='Фильтрация по дате публикации (начиная с ...)', required=False)
    to_datetime = serializers.DateTimeField(help_text='Фильтрация по дате публикации (до ...)', required=False)

    def validate(self, attrs):
        super().validate(attrs)

        lon_start = attrs.get('lon_start')
        lon_end = attrs.get('lon_end')
        lat_start = attrs.get('lat_start')
        lat_end = attrs.get('lat_end')

        # Если хотя бы один из параметров передан, необходимо проверить, чтобы все остальные параметры были
        if lon_start or lon_end or lat_start or lat_end:
            if not lon_start and not lon_end and not lat_start and not lat_end:
                raise serializers.ValidationError('Для фильтрации по координатам необходимо передать все 4 параметра:'
                                                  'lon_start, lon_end, lat_start, lat_end')
        return attrs

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    class Meta:
        fields = ('lon_start', 'lon_end', 'lat_start', 'lat_end', 'user_id', )


class PublicationSerializer(serializers.ModelSerializer):
    """
    Сериализатор модели публикации
    """
    user = serializers.SerializerMethodField(help_text='Владелец публикации', required=False)
    visibility_display = serializers.SerializerMethodField(help_text='Видимость поста для отображения')
    comments = serializers.SerializerMethodField(help_text='Последние комментарии')
    data = serializers.JSONField(help_text='Данные Unity объекта')
    likes_count = serializers.SerializerMethodField()
    dislikes_count = serializers.SerializerMethodField()

    @staticmethod
    def get_user(obj):
        serializer = PublicUserSerializer(obj.user)
        return serializer.data

    @staticmethod
    def get_comments(obj):
        """
        Получить последние 5 комментариев
        """
        comments = obj.comment_set.filter(is_deleted=False).order_by('-created')[:5]
        if not comments:
            return None
        serializer = CommentSerializer(comments, many=True)
        return serializer.data

    @staticmethod
    def get_visibility_display(obj):
        """
        Получить отображение типа видимости
        """
        return obj.get_visibility_display()

    @staticmethod
    def get_likes_count(obj):
        """
        Получить количество лайков публикации
        """
        return obj.votes.likes_count()

    @staticmethod
    def get_dislikes_count(obj):
        """
        Получить количество дизлайков публикации
        """
        return obj.votes.dislikes_count()

    def create(self, validated_data):
        user = self.context.get('user')
        publication = Publication(
            user=user,
            data=validated_data['data'],
            lat=validated_data['lat'],
            lon=validated_data['lon']
        )
        if validated_data.get('visibility'):
            publication.visibility = validated_data.get('visibility')
        publication.save()
        return publication

    class Meta:
        model = Publication
        fields = ('id', 'user', 'data', 'lat', 'lon', 'visibility', 'visibility_display', 'comments',
                  'likes_count', 'dislikes_count')
        read_only_fields = ('id', 'user', 'visibility_display', 'comments', 'likes_count', 'dislikes_count', )
