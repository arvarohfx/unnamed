from rest_framework import serializers


class PaginationRequestSerializer(serializers.Serializer):
    """
    Сериализатор запросов с пагинацией
    """
    offset = serializers.IntegerField(required=False)
    limit = serializers.IntegerField(required=False)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    class Meta:
        fields = ('offset', 'limit', )
        read_only_fields = fields
