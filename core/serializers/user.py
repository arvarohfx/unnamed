import phonenumbers
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from phonenumbers import NumberParseException
from rest_framework import serializers

from core.models import User
from core.models.common import VerificationCode
from core.utils import get_client_ip


class UserSerializer(serializers.ModelSerializer):
    """
    Сериализатор модели пользователя
    """
    password = serializers.CharField(
        label="Password",
        style={'input_type': 'password'},
        trim_whitespace=False,
        required=True
    )

    @staticmethod
    def validate_phone(phone):
        """
        Валидация номера телефона
        """
        if phone:
            try:
                phone = phonenumbers.parse(phone)
            except NumberParseException:
                raise serializers.ValidationError('Это не телефонный номер')
            if not phonenumbers.is_possible_number(phone):
                raise serializers.ValidationError('Невозможный телефонный номер: {}'.format(phone))
            if not phonenumbers.is_valid_number(phone):
                raise serializers.ValidationError('Некорректный телефонный номер')
        return phone

    @staticmethod
    def validate_email(email):
        """
        Валидация email
        :param email:
        :return:
        """
        try:
            validate_email(email)
        except ValidationError as e:
            raise serializers.ValidationError(e.messages[0])

        # Проверка: пользователь уже существует?
        users_count = User.objects.filter(email__iexact=email, is_active=True).count()
        if users_count > 0:
            raise serializers.ValidationError('Пользователь с таким email уже существует')
        return email

    @staticmethod
    def validate_password(password):
        """
        Валидация пароля, валидаторы настраиваются в settings.AUTH_PASSWORD_VALIDATORS
        :param password:
        :return:
        """
        try:
            validate_password(password)
        except ValidationError as e:  # не путать, это ValidationError из django
            # e.messages содержит список ошибок валидации, возвращаем первую
            raise serializers.ValidationError(e.messages[0])
        return password

    def create(self, validated_data):
        """
        Создание нового пользователя (Регистрация)
        :param validated_data:
        :return:
        """
        user = User.objects.create_user(
            email=validated_data['email'],
            username=validated_data['username'],
            password=validated_data['password'],
            first_name=validated_data['first_name'],
            phone=validated_data.get('phone'),
            middle_name=validated_data.get('middle_name'),
            last_name=validated_data.get('last_name'),
            avatar=validated_data.get('avatar'),
            gender=validated_data.get('gender'),
            birthday=validated_data.get('birthday'),
            is_active=False
        )
        # Создание и отправка кода подтверждения электронного адреса
        VerificationCode.create_short_email_verification_code(
            user.email, get_client_ip(self.context.get('request')))
        # TODO: Отправка кода верификации на электронный адрес
        return user

    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'password', 'phone', 'first_name', 'last_name', 'middle_name', 'gender',
                  'birthday', 'avatar', )
        read_only_fields = ('id',)
        required = ('email', 'password', 'first_name', 'username', )
        extra_kwargs = {
            'password': {'write_only': True},
        }


class PublicUserSerializer(serializers.ModelSerializer):
    """
    Публичный сериализатор пользователя
    """
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'middle_name', 'last_name', 'avatar', )
        read_only_fields = fields
