# Generated by Django 2.2.6 on 2019-12-20 11:30

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_friendrequest'),
    ]

    operations = [
        migrations.CreateModel(
            name='Friendship',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Время создания')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Время модификации')),
                ('friends', models.ManyToManyField(related_name='_friendship_friends_+', to='core.Friendship')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, related_name='friendship', to=settings.AUTH_USER_MODEL, verbose_name='Текущий пользователья')),
            ],
            options={
                'verbose_name': 'Дружба',
                'verbose_name_plural': 'Дружба',
            },
        ),
        migrations.CreateModel(
            name='FriendshipRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Время создания')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Время модификации')),
                ('message', models.CharField(blank=True, max_length=200, verbose_name='Сообщение')),
                ('is_accepted', models.BooleanField(default=False, verbose_name='Принят')),
                ('from_user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='invitations_from', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь, который отправлется запрос в друзья')),
                ('to_user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='invitations_to', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь, которому отправляется запрос в друзья')),
            ],
            options={
                'verbose_name': 'Запрос в друзья',
                'verbose_name_plural': 'Запросы в друзья',
                'ordering': ['-created'],
            },
        ),
        migrations.DeleteModel(
            name='FriendRequest',
        ),
    ]
