from django.conf import settings
from django.core.management.base import BaseCommand
from oauth2_provider.models import Application

from core.models import User


class Command(BaseCommand):

    @staticmethod
    def register_application():
        user = User.objects.get(pk=1, is_superuser=True)
        app = Application.objects.create(
            client_id=settings.OAUTH2_CLIENT_ID,
            user=user,
            client_type=Application.CLIENT_PUBLIC,
            authorization_grant_type=Application.GRANT_PASSWORD,
            client_secret=settings.OAUTH2_CLIENT_SECRET,
            name=settings.OAUTH2_APP_NAME
        )
        print('Создано приложение по-умолчанию: {}'.format(app.name))
        print('Client ID: {}'.format(app.client_id))
        print('Client Secret: {}'.format(app.client_secret))

    def handle(self, *args, **options):
        if Application.objects.count() == 0:
            self.register_application()
