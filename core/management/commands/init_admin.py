from decouple import config
from django.core.management.base import BaseCommand

from core.models import User


class Command(BaseCommand):

    def handle(self, *args, **options):
        if User.objects.count() == 0:
            email = config('DJANGO_SU_EMAIL', default='arvarohfx@gmail.com')
            password = config('DJANGO_SU_PASSWORD', default='admin')
            print('Creating account for %s. Current password is %s' % (email, password))
            admin = User.objects.create_superuser(
                email=email, password=password, first_name='Дмитрий', phone='+79091667370')
            admin.is_active = True
            admin.is_admin = True
            admin.save()
        else:
            print('Admin accounts can only be initialized if no User exist')
