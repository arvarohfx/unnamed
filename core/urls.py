from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.decorators import api_view

from core import views
# canonical base URL for the schema
from core.models import Publication
from core.models.comment import Comment

if settings.DEBUG:
    base_schema_url = None
else:
    base_schema_url = '{}/api/v1'.format(settings.BACKEND_URL)


swagger_info = openapi.Info(
    title="Unnamed API",
    default_version='v1',
    description="It's our well documented API. Feel free to ask questions.",  # noqa
    contact=openapi.Contact(name="Dmitriy Zotov", email="arvarohfx@gmail.com"),
    # terms_of_service="https://www.google.com/policies/terms/",
    # license=openapi.License(name="BSD License"),
)

SchemaView = get_schema_view(
    info=swagger_info,
    public=True,
    permission_classes=(permissions.AllowAny,),
    url=base_schema_url
)


@api_view(['GET'])
def plain_view(request):
    pass


required_urlpatterns = [
    url(r'^admin/', admin.site.urls),
]

urlpatterns = [
    # документация
    url(r'^swagger(?P<format>.json|.yaml)$', SchemaView.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', SchemaView.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', SchemaView.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    # редирект на документацию
    path('', RedirectView.as_view(pattern_name='schema-swagger-ui', permanent=False), name='index'),

    # API v1
    path('api/v1/', include([
        # Аутентификация oauth2
        url(r'^oauth/', include('oauth2_provider.urls', namespace='oauth2_provider')),
        path('oauth/token/', views.TokenAPIView.as_view(), name='oauth2_token'),

        # POST - Регистрация пользователя
        path('user/', views.UserRegister.as_view(), name='user_registration'),
        # GET - Получить пользователя по id
        # PUT - Получить обновить пользователя по id
        # PATCH - Частичный апдейт профиля по id
        path('user/<int:pk>/', views.UserView.as_view(), name='user_profile'),
        # POST - Подтверждение электронного адреса
        path('user/email_confirmation/', views.UserEmailConfirmation.as_view(), name='user_email_confirmation'),

        # POST - Восстановление пароля: отправка кода подтверждения электронного адреса
        # PATCH - Восстановление пароля: задание нового пароля
        path('user/password_reset/', views.UserPasswordReset.as_view(), name='user_password_reset'),

        # GET - Получить список всех публикаций
        # POST - Создать публикацию
        path('publication/', views.PublicationListView.as_view(), name='publication'),
        # GET - Получить детали публикации
        # PUT - Полное изменение публикации
        # PATCH - Частичное изменение публикации
        # DELETE - Удалить публикацию
        path('publication/<int:pk>/', views.PublicationView.as_view(), name='publication'),
        # POST - Оценить публикацию
        path('publication/<int:pk>/vote/', views.VoteView.as_view(model=Publication), name='publication_vote'),

        # GET - Получить список всех запросов в друзья
        # POST - Создать запрос на добавление в друзья
        path('friendship_request/', views.FriendRequestListView.as_view(), name='friendship_request'),
        # PATCH - Принять запрос на добавление в друзья
        # DELETE - Отклонить/Отменить запрос на добавление в друзья
        path('friendship_request/<int:pk>/', views.FriendRequestView.as_view(), name='friendship_request'),

        # GET - Получить список всех подписок и подписчиков
        # POST - Подписаться на пользователя
        path('follower/', views.FollowerListView.as_view(), name='follower'),
        # DELETE - Отписаться от пользователя
        path('follower/<int:user_id>/', views.FollowerView.as_view(), name='follower'),

        # GET - Получить список друзей
        path('friend/', views.FriendshipListView.as_view(), name='friendship'),
        # DELETE - Удалить пользователя из друзей
        path('friend/<int:user_id>/', views.FriendshipView.as_view(), name='friendship'),

        # GET - Получить список комментариев
        path('comment/', views.CommentListView.as_view(), name='comment'),
        # GET - Получить конкретный комментарий по id
        # POST - Добавить комментарий к публикации
        # PATCH - Редактировать комментарий к публикации
        # DELETE - Удалить комментарий к публикации
        path('comment/<int:pk>/', views.CommentView.as_view(), name='comment'),
        # POST - Оценить комментарий
        path('comment/<int:pk>/vote/', views.VoteView.as_view(model=Comment), name='comment_vote'),
    ])),

] + required_urlpatterns
