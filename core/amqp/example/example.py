from core.amqp.producer import BaseAMQPProducer


class ExampleProducer(BaseAMQPProducer):
    """
    Отправка в очередь данных
    """
    def publish(self, data):
        self._publish(data)
