import json
from datetime import datetime

from amqp import Message
from django.core.serializers.json import DjangoJSONEncoder


class AMQPMessage(Message):
    """
    Класс для базового JSON-сообщения с поддержкой дополнительных заголовков
    """
    def __init__(self, data, user_id, app_id, headers=None):
        data = json.dumps(data, sort_keys=True, indent=1, cls=DjangoJSONEncoder)
        properties = {
            'content_type': 'application/json',
            'content_encoding': 'utf-8',
            'delivery_mode': 2,
            'timestamp': int(datetime.now().timestamp()),
            'user_id': user_id,
            'app_id': app_id,
            'application_headers': {
                'published': int(datetime.now().timestamp()),
            }
        }
        if headers:
            properties['application_headers'].update(headers)
        super().__init__(data, **properties)
