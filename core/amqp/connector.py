import inspect
import logging
import os

import amqp
from django.conf import settings


logger = logging.getLogger(__name__)


class BaseAMQPConnector(object):
    """Базовый класс коннектора, от которого наследуюются продюссеры и консьюмеры"""

    class METHODS(object):
        # Описание смотри в BaseAMQPConsumer.process
        ACK = 'ack'
        ACK_R = 'ack_r'
        REJECT = 'reject'
        REQUEUE = 'requeue'

    _channel = None
    _connection = None

    def __new__(cls, *args, **kwargs):
        _curr_file = inspect.getfile(cls)
        cls = super().__new__(cls)
        # Генерация имени очереди на основе местоположения файла (directory.file.name),
        # где directory - имя exchange, file.name - имя файла с продюссером/консумером (file_name.py)
        if 'amqp' in _curr_file:
            cls.exchange_name = (os.path.basename(os.path.dirname(_curr_file))).replace('_', '.')
            cls.queue_name = os.path.splitext(os.path.basename(_curr_file))[0].replace('_', '.')
        else:
            cls.exchange_name = None
            cls.queue_name = None
        return cls

    def __init__(self, queue_name=None, auto_declaration=True):
        """
        Соединение с сервером.
        """
        try:
            self._connect()
        except OSError:
            return
        # Если передано имя очереди - используем его
        if queue_name:
            self.queue_name = queue_name
        # Создание exchange, queue, привязка queue к exchange
        if auto_declaration:
            self._declaration()

    def _close_channel(self):
        """
        Закрывает канал
        """
        try:
            self._channel.close()
        except Exception:
            pass

    def _close_connection(self):
        """
        Закрывает соединение
        """
        try:
            self._connection.close()
        except Exception:
            pass

    def __del__(self):
        """
        Закрывает канал и соединение
        """
        self._close_channel()
        self._close_connection()

    def _declaration(self):
        """Объявляем Exchange, очередь и биндим очередь на случай их отсутствия."""
        self._channel.exchange_declare(exchange=self.exchange_name, type='direct', durable=True, auto_delete=False)
        self._channel.queue_declare(queue=self.queue_name, durable=True, exclusive=False, auto_delete=False)
        self._channel.queue_bind(exchange=self.exchange_name, queue=self.queue_name, routing_key=self.queue_name)

    def _connect(self):
        """Соединение с сервером, получение свободного канала."""
        self._connection = amqp.Connection(
            host='{}:{}'.format(settings.RMQ_HOST, settings.RMQ_PORT),
            userid=settings.RMQ_USER,
            password=settings.RMQ_PASSWORD,
            virtual_host=settings.RMQ_VIRTUAL_HOST
        )
        self._connection.connect()
        self._channel = self._connection.channel()

    def get_queue_size(self, queue_name=None):
        """
        Получить кол-во сообщений в очереди в текущем exchange.
        Если параметр queue_name не задан, используется self.queue_name
        """
        queue_name = queue_name if queue_name else self.queue_name
        queue_name, queue_size, consumer_count = self._channel.queue_declare(queue=queue_name, passive=True)
        return queue_size

    def purge(self, nowait=False):
        """Если nowait указан False, то возвращается кол-во сообщение в очереди"""
        return self._channel.queue_purge(self.queue_name, nowait=nowait)
