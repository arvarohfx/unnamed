from django.conf import settings

from core.amqp.connector import BaseAMQPConnector
from core.amqp.message import AMQPMessage


class BaseAMQPProducer(BaseAMQPConnector):
    """Базовый класс продюсера"""

    def __init__(self, app_id, queue_name, auto_declaration):
        super().__init__(queue_name=queue_name, auto_declaration=auto_declaration)
        self.app_id = app_id

    def _publish(self, data):
        """Отправка сообщения в очередь, если есть открытый канал"""
        if self._channel:
            self._channel.basic_publish(
                AMQPMessage(data, settings.RMQ_USER, self.app_id),
                exchange=self.exchange_name,
                routing_key=self.queue_name,
                timeout=600
            )
