import re
import string
from random import choice


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def is_valid_email(email):
    """Валидация электронного адреса"""
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


class StringUtils(object):
    FULL_CHARS_SET = string.ascii_letters + string.digits
    MOBILE_CHARS_SET = '0123456789'
    re_phone = re.compile(r'[^\d]')

    @staticmethod
    def random_string(chars, length):
        return "".join(choice(chars) for _ in range(length))

    @classmethod
    def generate_digit_code(cls, length=4):
        return cls.random_string(cls.MOBILE_CHARS_SET, length)

    @classmethod
    def generate_mobile_code(cls):
        return cls.random_string(cls.MOBILE_CHARS_SET, 4)

    @classmethod
    def generate_email_code(cls):
        return cls.random_string(cls.FULL_CHARS_SET, 65)

    @classmethod
    def normalize_phone(cls, phone_number):
        if not phone_number:
            return phone_number
        return cls.re_phone.sub('', str(phone_number))

    @classmethod
    def generate_password(cls):
        return cls.random_string(cls.FULL_CHARS_SET, 8)
