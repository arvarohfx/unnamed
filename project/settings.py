import os

from decouple import config
from django.templatetags.static import static
from django.urls import reverse_lazy
from django.utils.functional import lazy

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config('DJANGO_DEBUG', default=False, cast=bool)


ALLOWED_HOSTS = config('DJANGO_ALLOWED_HOSTS', default='localhost, 127.0.0.1, [::1]',
                       cast=lambda v: [s.strip() for s in v.split(',')])

# SENTRY SETTINGS
# if not DEBUG:
#     sentry_sdk.init(
#         dsn="https://412832cf37574f0a97719ed10eb0fa7a@sentry.io/1661868",
#         integrations=[DjangoIntegration()]
#     )

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config('DJANGO_SECRET_KEY', default='2szv+01+zk1!m(1r_d$2st+fw188o_n(u(d&w*$rr_v)yvo#2p')


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

WSGI_APPLICATION = 'project.wsgi.application'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

DATABASES = {
    'default': {
        'ENGINE': config('DJANGO_DB_ENGINE', default='django.db.backends.sqlite3'),
        'NAME': config('DJANGO_DB_NAME', default=os.path.join(BASE_DIR, 'db.sqlite3')),
        'USER': config('DJANGO_DB_USER', default=''),
        'PASSWORD': config('DJANGO_DB_PASSWORD', default=''),
        'HOST': config('DJANGO_DB_HOST', default=''),
        'PORT': config('DJANGO_DB_PORT', default='5432')
    }
}

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'oauth2_provider',
    'corsheaders',
    'drf_yasg',

    'core',
]

CORS_ORIGIN_ALLOW_ALL = True

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'drf_yasg.middleware.SwaggerExceptionMiddleware',
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    )
}

OAUTH2_PROVIDER = {
    # this is the list of available scopes
    'SCOPES': {'read': 'Read scope', 'write': 'Write scope', 'groups': 'Access to your groups'}
}

ROOT_URLCONF = 'project.urls'

# Переопределяем модель пользователя на свою собственную
AUTH_USER_MODEL = 'core.User'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 8,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader'
            ]
        },
    },
]

OAUTH2_CLIENT_ID = 'N1Ua4klfvCcFRdYaIMl12eGINW8A6McFdFwDQTsO'
OAUTH2_CLIENT_SECRET = 'MSu3e4sYFuyZkqwOxO3EG6hnPuvC7qvfadte2v81wXvV7rfBP9z4fyYVBp4TWN4Ps0y2OpyI9asFGc3n1VvceUa3tmHqBH5mQmV891FVeh7ZtNqH8AuEvmpYQBa8h6P2'
OAUTH2_APP_NAME = 'boARd'

OAUTH2_REDIRECT_URL = lazy(static, 'drf-yasg/swagger-ui-dist/oauth2-redirect.html')
OAUTH2_AUTHORIZE_URL = reverse_lazy('oauth2_provider:authorize')
OAUTH2_TOKEN_URL = reverse_lazy('oauth2_provider:token')

# drf-yasg
SWAGGER_SETTINGS = {
    'LOGIN_URL': reverse_lazy('admin:login'),
    'LOGOUT_URL': '/admin/logout',
    'PERSIST_AUTH': True,
    'REFETCH_SCHEMA_WITH_AUTH': True,
    'REFETCH_SCHEMA_ON_LOGOUT': True,

    'DEFAULT_INFO': 'project.urls.swagger_info',

    'SECURITY_DEFINITIONS': {
        'OAuth2 password': {
            'flow': 'password',
            'scopes': {
                'read': 'Read everything.',
                'write': 'Write everything,',
            },
            'tokenUrl': OAUTH2_TOKEN_URL,
            'type': 'oauth2',
        },
        # 'Basic': {
        #     'type': 'basic'
        # }
    },
    'OAUTH2_REDIRECT_URL': OAUTH2_REDIRECT_URL,
    'OAUTH2_CONFIG': {
        'clientId': OAUTH2_CLIENT_ID,
        'clientSecret': OAUTH2_CLIENT_SECRET,
        'appName': OAUTH2_APP_NAME,
    },
    "DEFAULT_PAGINATOR_INSPECTORS": [
        'drf_yasg.inspectors.DjangoRestResponsePagination',
        'drf_yasg.inspectors.CoreAPICompatInspector',
    ]
}

REDOC_SETTINGS = {
    'SPEC_URL': ('schema-json', {'format': '.json'}),
}

MEDIA_URL = config('DJANGO_MEDIA_URL', default='/media/')
MEDIA_ROOT = config('DJANGO_MEDIA_ROOT', default='/srv/media/')

STATIC_URL = config('DJANGO_STATIC_URL', default='/static/')
STATIC_ROOT = config('DJANGO_STATIC_ROOT', default='/srv/static/')

BACKEND_SCHEMA = config('DJANGO_BACKEND_SCHEMA', default='http://')
BACKEND_URL = config('DJANGO_BACKEND_URL', default='127.0.0.1')
BACKEND_URL = BACKEND_SCHEMA + BACKEND_URL

# RMQ
RMQ_HOST = config('DJANGO_RMQ_HOST', default='rabbit')
RMQ_PORT = config('DJANGO_RMQ_PORT', default=5672, cast=int)
RMQ_USER = config('DJANGO_RMQ_USER', default='admin')
RMQ_PASSWORD = config('DJANGO_RMQ_PASSWORD', default='awYfPPzbJVJ4HR')
RMQ_VIRTUAL_HOST = config('DJANGO_RMQ_VIRTUAL_HOST', default='/')
# END RMQ
