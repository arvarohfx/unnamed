FROM python:3.6.8-slim
# переменные окружения
ENV PYTHONUNBUFFERED=1 DJANGO_SETTINGS_MODULE=project.settings
#ENV DJANGO_SU_EMAIL=admin@%APP_NAME%.com DJANGO_SU_PASSWORD=admin
ENV GUNICORN_CMD_ARGS="--bind=:8000 --workers=2"
ENV PIPENV_VENV_IN_PROJECT=1

# системные зависимости (libpq-dev gcc python3-dev - для установки psycopg2)
RUN set -x \
 # устанавливаем зависимости для сборки проекта
 && export REQUIRED_PACKAGES="libpq-dev gcc python3-dev" \
 && apt-get update \
 && apt-get install --no-install-recommends -y $REQUIRED_PACKAGES \
 && rm -rf /var/lib/apt/lists/* \
 # устанавливаем pipenv
 && pip install --upgrade pip \
 && pip install --upgrade pipenv

WORKDIR /code

# до тех пор, пока Pipfile не изменится, этот слой будет закэширован
COPY Pipfile Pipfile
COPY Pipfile.lock Pipfile.lock

RUN set -x \
 # установка backend зависимостей с помощью pipenv
 # 1. You need to use --system flag, so it will install all packages into the system python, and not into the virtualenv. Since docker containers do not need to have virtualenvs
 # 2. You need to use --deploy flag, so your build will fail if your Pipfile.lock is out of date
 # 3. You need to use --ignore-pipfile, so it won't mess with our setup
 && pipenv install --three --deploy --ignore-pipfile

# копируем исходники внутрь контейнера
ADD . /code/

# # удаляем пакеты, которые были нужны для сборки
# && apt-get remove -y $REQUIRED_PACKAGES \
# && apt-get autoremove -y \
# && apt-get clean

EXPOSE 8000
# запускаем gunicorn
CMD ["pipenv", "run", "gunicorn", "project.wsgi"]
